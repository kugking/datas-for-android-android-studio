package kavita.zocial365.app.datas.Model;

/**
 * Created by kugking on 1/12/15 AD.
 */
public class UserModel {
    private String username="username",spp_id="spp_id",user_level_id="user_level_id",name="name",status="status",create_date="create_date",edit_date="edit_date",auth_token="auth_token";

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSpp_id() {
        return spp_id;
    }

    public void setSpp_id(String spp_id) {
        this.spp_id = spp_id;
    }

    public String getUser_level_id() {
        return user_level_id;
    }

    public void setUser_level_id(String user_level_id) {
        this.user_level_id = user_level_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public String getEdit_date() {
        return edit_date;
    }

    public void setEdit_date(String edit_date) {
        this.edit_date = edit_date;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }
}
