package kavita.zocial365.app.datas.http;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.squareup.okhttp.Response;

import java.io.IOException;

/**
 * Created by nuuneoi on 10/20/2014.
 */
public class HTTPRequestTask extends AsyncTask<HTTPRequestData, Void, HTTPRequestTask.ContentMessage> {

    HTTPRequestListener mListener;
    Activity activity = null;
    private ProgressDialog progressDialog;

    public HTTPRequestTask(HTTPRequestListener aListener) {
        mListener = aListener;
    }

    public HTTPRequestTask(HTTPRequestListener aListener, Activity activity) {
        mListener = aListener;
        this.activity = activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (activity != null) {
            try {
                progressDialog = new ProgressDialog(activity);
                progressDialog.setTitle("Downloading...");
                progressDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    @Override
    protected ContentMessage doInBackground(HTTPRequestData... params) {
        HTTPRequestData data = params[0];
        ContentMessage message = new ContentMessage();

        try {
            Response response = data.call.execute();
            if (response.isSuccessful()) {
                message.success = true;
                message.statusCode = response.code();
            } else {
                message.success = false;
                message.statusCode = response.code();
            }
            message.body = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
            message.success = false;
        }

        return message;
    }

    @Override
    protected void onPostExecute(ContentMessage s) {
        super.onPostExecute(s);

        if (mListener != null) {
            if (s.success) {
                mListener.onMessageReceived(s.body);
            } else {
                mListener.onMessageError(s.statusCode, s.body);
            }
        }
        if (activity != null) {
            if (progressDialog != null) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        }


    }

    public class ContentMessage {
        boolean success;
        int statusCode;
        String body;
    }
}
