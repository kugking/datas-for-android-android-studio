package kavita.zocial365.app.datas.Model;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by kugking on 12/14/14 AD.
 */
public class EvaluateModel {
    String id = "id", sequen_no = "sequen_no", no = "no", description = "description", parent_id = "parent_id",
            total_score = "total_score",
            have_choice = "have_choice",
            choice_type = "choice_type",
            remark = "remark",
            rules = "rules",
            lastchild = "lastchild",
            year = "year",tour_id="tour_id",topic_general_id="topic_general_id",choice_weight="choice_weight",choice_val="choice_val";
    Boolean checked = false;
    JSONArray topic_general;

    ArrayList<EvaluateModel> subEvaluateModels = new ArrayList<EvaluateModel>();

    public String getChoice_val() {
        return choice_val;
    }

    public void setChoice_val(String choice_val) {
        this.choice_val = choice_val;
    }

    public Boolean isChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public JSONArray getTopic_general() {
        return topic_general;
    }

    public void setTopic_general(JSONArray topic_general) {
        this.topic_general = topic_general;
    }

    public void addSubEvaluate(EvaluateModel subEvaluateModel) {
        subEvaluateModels.add(subEvaluateModel);
    }

    public ArrayList<EvaluateModel> getSubEvaluateModels() {
        return subEvaluateModels;
    }

    public void setSubEvaluateModels(ArrayList<EvaluateModel> subEvaluateModels) {
        this.subEvaluateModels = subEvaluateModels;
    }

    public String getTopic_general_id() {
        return topic_general_id;
    }

    public void setTopic_general_id(String topic_general_id) {
        this.topic_general_id = topic_general_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSequen_no() {
        return sequen_no;
    }

    public void setSequen_no(String sequen_no) {
        this.sequen_no = sequen_no;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getTotal_score() {
        return total_score;
    }

    public void setTotal_score(String total_score) {
        this.total_score = total_score;
    }

    public String getHave_choice() {
        return have_choice;
    }

    public void setHave_choice(String have_choice) {
        this.have_choice = have_choice;
    }

    public String getChoice_type() {
        return choice_type;
    }

    public void setChoice_type(String choice_type) {
        this.choice_type = choice_type;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRules() {
        return rules;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }

    public String getLastchild() {
        return lastchild;
    }

    public void setLastchild(String lastchild) {
        this.lastchild = lastchild;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
