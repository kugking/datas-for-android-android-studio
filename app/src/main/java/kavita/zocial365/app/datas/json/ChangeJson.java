package kavita.zocial365.app.datas.json;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import kavita.zocial365.app.datas.Model.ImagesModel;
import kavita.zocial365.app.datas.Model.TouristModel;

/**
 * Created by kugking on 12/3/14 AD.
 */
public class ChangeJson {
    public ArrayList<TouristModel> addTouristModel(JSONArray jsonArray) {
        ArrayList<TouristModel> touristModels1 = new ArrayList<TouristModel>();
        String tour_id = "tour_id", name = "name", name_form = "name_form", care_agen = "care_agen", date_create = "date_create", type_id = "type_id", type_type_nm_t = "type_type_nm_t",
                sec_type_nm_t = "sec_type_nm_t", type_sub_nm_t = "type_sub_nm_t", description = "description",
                tour_day = "tour_day", time_open = "time_open", time_close = "time_close",
                addr = "addr", road = "road", thumbon_id = "thumbon_id", amphur_id = "amphur_id", province_id = "province_id", region_id = "region_id", zipcode = "zipcode",
                tel = "tes", fax = "fax", website = "website", own_land = "own_land", own_land_right = "own_land_right",
                lot_discribe = "lot_discribe", manage_agen = "manage_agen", loc_or_no1 = "loc_or_no1", th_tour_dialy = "th_tour_dialy",
                status = "status", dasta_id = "dasta_id", status_low_carbon = "status_low_carbon", edit_date = "edit_date", user_id = "user_id", id = "id", fee_thai_child = "fee_thai_child",
                fee_thai_adult = "fee_thai_adult", fee_forient_child = "fee_forient_child", fee_forient_adult = "fee_forient_adult", degreex = "degreex", degreey = "degreey",
                th_tour_monthly = "th_tour_monthly", th_tour_yearly = "th_tour_yearly", for_tour_daily = "for_tour_daily",
                for_tour_monthly = "for_tour_monthly", for_tour_yearly = "for_tour_yearly", gid = "gid", tbl_tourist_image = "tbl_tourist_image", tbl_tourist_eva_summary = "tbl_tourist_eva_summary";
        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject jsonObject = jsonArray.optJSONObject(i);

//            if(jsonObject.optString(id).equalsIgnoreCase(touristModels.get()))
            TouristModel touristModel = new TouristModel();
            touristModel.setId(jsonObject.optInt(id));
            touristModel.setTour_id(jsonObject.optString(tour_id));
            touristModel.setName(jsonObject.optString(name));
            touristModel.setName_form(jsonObject.optString(name_form));
            touristModel.setCare_agen(jsonObject.optString(care_agen));
            touristModel.setDate_create(jsonObject.optString(date_create));
            touristModel.setType_id(jsonObject.optString(type_id));
            touristModel.setType_type_nm_t(jsonObject.optString(type_type_nm_t));
            touristModel.setSec_type_nm_t(jsonObject.optString(sec_type_nm_t));
            touristModel.setType_sub_nm_t(jsonObject.optString(type_sub_nm_t));
            touristModel.setDescription(jsonObject.optString(description));
            touristModel.setTour_day(jsonObject.optString(tour_day));

            touristModel.setTime_open(jsonObject.optString(time_open));
            touristModel.setTime_close(jsonObject.optString(time_close));

            touristModel.setFee_thai_child(jsonObject.optInt(fee_thai_child));
            touristModel.setFee_thai_adult(jsonObject.optInt(fee_thai_adult));
            touristModel.setFee_forient_child(jsonObject.optInt(fee_forient_child));
            touristModel.setFee_forient_adult(jsonObject.optInt(fee_forient_adult));
            touristModel.setDegreex(jsonObject.optDouble(degreex));
            touristModel.setDegreey(jsonObject.optDouble(degreey));
            touristModel.setAddr(jsonObject.optString(addr));
            touristModel.setRoad(jsonObject.optString(road));
            touristModel.setThumbon_id(jsonObject.optString(thumbon_id));
            touristModel.setAmphur_id(jsonObject.optString(amphur_id));
            touristModel.setProvince_id(jsonObject.optString(province_id));
            touristModel.setRegion_id(jsonObject.optString(region_id));
            touristModel.setZipcode(jsonObject.optString(zipcode));
            touristModel.setTel(jsonObject.optString(tel));
            touristModel.setFax(jsonObject.optString(fax));
            touristModel.setWebsite(jsonObject.optString(website));
            touristModel.setOwn_land(jsonObject.optString(own_land));
            touristModel.setOwn_land_right(jsonObject.optString(own_land_right));
            touristModel.setLot_discribe(jsonObject.optString(lot_discribe));
            touristModel.setManage_agen(jsonObject.optString(manage_agen));
            touristModel.setLoc_or_no1(jsonObject.optString(loc_or_no1));
            touristModel.setTh_tour_dialy(jsonObject.optInt(th_tour_dialy));
            touristModel.setTh_tour_monthly(jsonObject.optInt(th_tour_monthly));
            touristModel.setTh_tour_yearly(jsonObject.optInt(th_tour_yearly));
            touristModel.setFor_tour_daily(jsonObject.optInt(for_tour_daily));
            touristModel.setFor_tour_monthly(jsonObject.optInt(for_tour_monthly));
            touristModel.setFor_tour_yearly(jsonObject.optInt(for_tour_yearly));
            touristModel.setStatus(jsonObject.optString(status));
            touristModel.setGid(jsonObject.optInt(gid));
            touristModel.setDasta_id(jsonObject.optString(dasta_id));
            touristModel.setStatus_low_carbon(jsonObject.optString(status_low_carbon));
            touristModel.setEdit_date(jsonObject.optString(edit_date));
            touristModel.setUser_id(jsonObject.optString(user_id));
            touristModel.setTbl_tourist_eva_summary(jsonObject.optJSONArray(tbl_tourist_eva_summary));

            JSONArray jsonArray2 = jsonObject.optJSONArray(tbl_tourist_image);
            for (int p = 0; p < jsonArray2.length(); p++) {
                JSONObject jsonArray1 = jsonArray2.optJSONObject(p);
                ImagesModel imagesModel = new ImagesModel();

                imagesModel.setId(jsonArray1.optString("id"));
                imagesModel.setImg_id(jsonArray1.optString("img_id"));
                imagesModel.setTour_id(jsonArray1.optString("tour_id"));
                imagesModel.setPath(jsonArray1.optString("path"));
                imagesModel.setImage_url(jsonArray1.optString("image_url"));
                imagesModel.setImage_thumb_url(jsonArray1.optString("image_thumb_url"));

                touristModel.addImageModel(imagesModel);
            }

            touristModels1.add(touristModel);
        }
        return touristModels1;
    }

    public TouristModel addTouristModel(JSONObject jsonObject) {
        TouristModel touristModel = new TouristModel();
        String tour_id = "tour_id", name = "name", name_form = "name_form", care_agen = "care_agen", date_create = "date_create", type_id = "type_id", type_type_nm_t = "type_type_nm_t",
                sec_type_nm_t = "sec_type_nm_t", type_sub_nm_t = "type_sub_nm_t", description = "description",
                tour_day = "tour_day", time_open = "time_open", time_close = "time_close",
                addr = "addr", road = "road", thumbon_id = "thumbon_id", amphur_id = "amphur_id", province_id = "province_id", region_id = "region_id", zipcode = "zipcode",
                tel = "tes", fax = "fax", website = "website", own_land = "own_land", own_land_right = "own_land_right",
                lot_discribe = "lot_discribe", manage_agen = "manage_agen", loc_or_no1 = "loc_or_no1", th_tour_dialy = "th_tour_dialy",
                status = "status", dasta_id = "dasta_id", status_low_carbon = "status_low_carbon", edit_date = "edit_date", user_id = "user_id", id = "id", fee_thai_child = "fee_thai_child",
                fee_thai_adult = "fee_thai_adult", fee_forient_child = "fee_forient_child", fee_forient_adult = "fee_forient_adult", degreex = "degreex", degreey = "degreey",
                th_tour_monthly = "th_tour_monthly", th_tour_yearly = "th_tour_yearly", for_tour_daily = "for_tour_daily",
                for_tour_monthly = "for_tour_monthly", for_tour_yearly = "for_tour_yearly", gid = "gid", tbl_tourist_image = "tbl_tourist_image", tbl_tourist_eva_summary = "tbl_tourist_eva_summary";
        try {


//            if(jsonObject.optString(id).equalsIgnoreCase(touristModels.get()))

            touristModel.setId(jsonObject.optInt(id));
            touristModel.setTour_id(jsonObject.optString(tour_id));
            touristModel.setName(jsonObject.optString(name));
            touristModel.setName_form(jsonObject.optString(name_form));
            touristModel.setCare_agen(jsonObject.optString(care_agen));
            touristModel.setDate_create(jsonObject.optString(date_create));
            touristModel.setType_id(jsonObject.optString(type_id));
            touristModel.setType_type_nm_t(jsonObject.optString(type_type_nm_t));
            touristModel.setSec_type_nm_t(jsonObject.optString(sec_type_nm_t));
            touristModel.setType_sub_nm_t(jsonObject.optString(type_sub_nm_t));
            touristModel.setDescription(jsonObject.optString(description));
            touristModel.setTour_day(jsonObject.optString(tour_day));
            touristModel.setTime_open(jsonObject.optString(time_open));
            touristModel.setTime_close(jsonObject.optString(time_close));
            touristModel.setFee_thai_child(jsonObject.optInt(fee_thai_child));
            touristModel.setFee_thai_adult(jsonObject.optInt(fee_thai_adult));
            touristModel.setFee_forient_child(jsonObject.optInt(fee_forient_child));
            touristModel.setFee_forient_adult(jsonObject.optInt(fee_forient_adult));
            touristModel.setDegreex(jsonObject.optDouble(degreex));
            touristModel.setDegreey(jsonObject.optDouble(degreey));
            touristModel.setAddr(jsonObject.optString(addr));
            touristModel.setRoad(jsonObject.optString(road));
            touristModel.setThumbon_id(jsonObject.optString(thumbon_id));
            touristModel.setAmphur_id(jsonObject.optString(amphur_id));
            touristModel.setProvince_id(jsonObject.optString(province_id));
            touristModel.setRegion_id(jsonObject.optString(region_id));
            touristModel.setZipcode(jsonObject.optString(zipcode));
            touristModel.setTel(jsonObject.optString(tel));
            touristModel.setFax(jsonObject.optString(fax));
            touristModel.setWebsite(jsonObject.optString(website));
            touristModel.setOwn_land(jsonObject.optString(own_land));
            touristModel.setOwn_land_right(jsonObject.optString(own_land_right));
            touristModel.setLot_discribe(jsonObject.optString(lot_discribe));
            touristModel.setManage_agen(jsonObject.optString(manage_agen));
            touristModel.setLoc_or_no1(jsonObject.optString(loc_or_no1));
            touristModel.setTh_tour_dialy(jsonObject.optInt(th_tour_dialy));
            touristModel.setTh_tour_monthly(jsonObject.optInt(th_tour_monthly));
            touristModel.setTh_tour_yearly(jsonObject.optInt(th_tour_yearly));
            touristModel.setFor_tour_daily(jsonObject.optInt(for_tour_daily));
            touristModel.setFor_tour_monthly(jsonObject.optInt(for_tour_monthly));
            touristModel.setFor_tour_yearly(jsonObject.optInt(for_tour_yearly));
            touristModel.setStatus(jsonObject.optString(status));
            touristModel.setGid(jsonObject.optInt(gid));
            touristModel.setDasta_id(jsonObject.optString(dasta_id));
            touristModel.setStatus_low_carbon(jsonObject.optString(status_low_carbon));
            touristModel.setEdit_date(jsonObject.optString(edit_date));
            touristModel.setUser_id(jsonObject.optString(user_id));
            touristModel.setTbl_tourist_eva_summary(jsonObject.optJSONArray(tbl_tourist_eva_summary));

            JSONArray jsonArray2 = jsonObject.optJSONArray(tbl_tourist_image);
            for (int p = 0; p < jsonArray2.length(); p++) {
                JSONObject jsonArray1 = jsonArray2.optJSONObject(p);
                ImagesModel imagesModel = new ImagesModel();

                imagesModel.setId(jsonArray1.optString("id"));
                imagesModel.setImg_id(jsonArray1.optString("img_id"));
                imagesModel.setTour_id(jsonArray1.optString("tour_id"));
                imagesModel.setPath(jsonArray1.optString("path"));
                imagesModel.setImage_url(jsonArray1.optString("image_url"));
                imagesModel.setImage_thumb_url(jsonArray1.optString("image_thumb_url"));

                touristModel.addImageModel(imagesModel);
            }
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return touristModel;


    }
}
