package kavita.zocial365.app.datas.Model;

import com.google.android.gms.maps.model.Marker;

import org.json.JSONArray;

import java.sql.Array;
import java.util.ArrayList;

/**
 * Created by kugking on 12/2/14 AD.
 */
public class TouristModel {
    private String tour_id = "", name = "", name_form = "", care_agen = "", date_create = "", type_id = "", type_type_nm_t = "", sec_type_nm_t = "", type_sub_nm_t = "", description = "",
            tour_day = "", time_open = "", time_close = "",
            addr = "", road = "", thumbon_id = "", amphur_id = "", province_id = "", region_id = "", zipcode = "", tel = "", fax = "", website = "", own_land = "", own_land_right = "",
            lot_discribe = "", manage_agen = "", loc_or_no1 = "",
            status = "", dasta_id = "", status_low_carbon = "", edit_date = "", user_id = "";
    JSONArray tbl_tourist_eva_summary;
    private int id = 0, fee_thai_child = 0, fee_thai_adult = 0, fee_forient_child = 0, fee_forient_adult = 0, th_tour_monthly = 0, th_tour_yearly = 0, for_tour_daily = 0,
            for_tour_monthly = 0, for_tour_yearly = 0, gid = 0, th_tour_dialy = 0;
    private Double degreex = 0.0, degreey = 0.0;
    private Marker marker = null;
    private ArrayList<ImagesModel> imagesModels = new ArrayList<ImagesModel>();

//    String id_img = "id", img_id = "img_id", path_img = "path";

//    public String getId_img() {
//        return id_img;
//    }
//
//    public void setId_img(String id_img) {
//        this.id_img = id_img;
//    }
//
//    public String getImg_id() {
//        return img_id;
//    }
//
//    public void setImg_id(String img_id) {
//        this.img_id = img_id;
//    }
//
//    public String getPath_img() {
//        return path_img;
//    }
//
//    public void setPath_img(String path_img) {
//        this.path_img = path_img;
//    }


    public JSONArray getTbl_tourist_eva_summary() {
        return tbl_tourist_eva_summary;
    }

    public void setTbl_tourist_eva_summary(JSONArray tbl_tourist_eva_summary) {
        this.tbl_tourist_eva_summary = tbl_tourist_eva_summary;
    }

    public ArrayList<ImagesModel> getImagesModels() {
        return imagesModels;
    }

    public void addImageModel(ImagesModel imagesModel) {
        imagesModels.add(imagesModel);
    }

    public void remove(int position) {
        imagesModels.remove(position);
    }

    public void delete() {
        imagesModels.clear();
    }

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }

    public String getTour_id() {
        return tour_id;
    }

    public void setTour_id(String tour_id) {
        this.tour_id = tour_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName_form() {
        return name_form;
    }

    public void setName_form(String name_form) {
        this.name_form = name_form;
    }

    public String getCare_agen() {
        return care_agen;
    }

    public void setCare_agen(String care_agen) {
        this.care_agen = care_agen;
    }

    public String getDate_create() {
        return date_create;
    }

    public void setDate_create(String date_create) {
        this.date_create = date_create;
    }

    public String getType_id() {
        return type_id;
    }

    public void setType_id(String type_id) {
        this.type_id = type_id;
    }

    public String getType_type_nm_t() {
        return type_type_nm_t;
    }

    public void setType_type_nm_t(String type_type_nm_t) {
        this.type_type_nm_t = type_type_nm_t;
    }

    public String getSec_type_nm_t() {
        return sec_type_nm_t;
    }

    public void setSec_type_nm_t(String sec_type_nm_t) {
        this.sec_type_nm_t = sec_type_nm_t;
    }

    public String getType_sub_nm_t() {
        return type_sub_nm_t;
    }

    public void setType_sub_nm_t(String type_sub_nm_t) {
        this.type_sub_nm_t = type_sub_nm_t;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTour_day() {
        return tour_day;
    }

    public void setTour_day(String tour_day) {
        this.tour_day = tour_day;
    }

    public String getTime_open() {
        return time_open;
    }

    public void setTime_open(String time_open) {
        this.time_open = time_open;
    }

    public String getTime_close() {
        return time_close;
    }

    public void setTime_close(String time_close) {
        this.time_close = time_close;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public String getThumbon_id() {
        return thumbon_id;
    }

    public void setThumbon_id(String thumbon_id) {
        this.thumbon_id = thumbon_id;
    }

    public String getAmphur_id() {
        return amphur_id;
    }

    public void setAmphur_id(String amphur_id) {
        this.amphur_id = amphur_id;
    }

    public String getProvince_id() {
        return province_id;
    }

    public void setProvince_id(String province_id) {
        this.province_id = province_id;
    }

    public String getRegion_id() {
        return region_id;
    }

    public void setRegion_id(String region_id) {
        this.region_id = region_id;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getOwn_land() {
        return own_land;
    }

    public void setOwn_land(String own_land) {
        this.own_land = own_land;
    }

    public String getOwn_land_right() {
        return own_land_right;
    }

    public void setOwn_land_right(String own_land_right) {
        this.own_land_right = own_land_right;
    }

    public String getLot_discribe() {
        return lot_discribe;
    }

    public void setLot_discribe(String lot_discribe) {
        this.lot_discribe = lot_discribe;
    }

    public String getManage_agen() {
        return manage_agen;
    }

    public void setManage_agen(String manage_agen) {
        this.manage_agen = manage_agen;
    }

    public String getLoc_or_no1() {
        return loc_or_no1;
    }

    public void setLoc_or_no1(String loc_or_no1) {
        this.loc_or_no1 = loc_or_no1;
    }

    public int getTh_tour_dialy() {
        return th_tour_dialy;
    }

    public void setTh_tour_dialy(int th_tour_dialy) {
        this.th_tour_dialy = th_tour_dialy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDasta_id() {
        return dasta_id;
    }

    public void setDasta_id(String dasta_id) {
        this.dasta_id = dasta_id;
    }

    public String getStatus_low_carbon() {
        return status_low_carbon;
    }

    public void setStatus_low_carbon(String status_low_carbon) {
        this.status_low_carbon = status_low_carbon;
    }

    public String getEdit_date() {
        return edit_date;
    }

    public void setEdit_date(String edit_date) {
        this.edit_date = edit_date;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFee_thai_child() {
        return fee_thai_child;
    }

    public void setFee_thai_child(int fee_thai_child) {
        this.fee_thai_child = fee_thai_child;
    }

    public int getFee_thai_adult() {
        return fee_thai_adult;
    }

    public void setFee_thai_adult(int fee_thai_adult) {
        this.fee_thai_adult = fee_thai_adult;
    }

    public int getFee_forient_child() {
        return fee_forient_child;
    }

    public void setFee_forient_child(int fee_forient_child) {
        this.fee_forient_child = fee_forient_child;
    }

    public int getFee_forient_adult() {
        return fee_forient_adult;
    }

    public void setFee_forient_adult(int fee_forient_adult) {
        this.fee_forient_adult = fee_forient_adult;
    }

    public Double getDegreex() {
        return degreex;
    }

    public void setDegreex(Double degreex) {
        this.degreex = degreex;
    }

    public Double getDegreey() {
        return degreey;
    }

    public void setDegreey(Double degreey) {
        this.degreey = degreey;
    }

    public int getTh_tour_monthly() {
        return th_tour_monthly;
    }

    public void setTh_tour_monthly(int th_tour_monthly) {
        this.th_tour_monthly = th_tour_monthly;
    }

    public int getTh_tour_yearly() {
        return th_tour_yearly;
    }

    public void setTh_tour_yearly(int th_tour_yearly) {
        this.th_tour_yearly = th_tour_yearly;
    }

    public int getFor_tour_daily() {
        return for_tour_daily;
    }

    public void setFor_tour_daily(int for_tour_daily) {
        this.for_tour_daily = for_tour_daily;
    }

    public int getFor_tour_monthly() {
        return for_tour_monthly;
    }

    public void setFor_tour_monthly(int for_tour_monthly) {
        this.for_tour_monthly = for_tour_monthly;
    }

    public int getFor_tour_yearly() {
        return for_tour_yearly;
    }

    public void setFor_tour_yearly(int for_tour_yearly) {
        this.for_tour_yearly = for_tour_yearly;
    }

    public int getGid() {
        return gid;
    }

    public void setGid(int gid) {
        this.gid = gid;
    }
}
