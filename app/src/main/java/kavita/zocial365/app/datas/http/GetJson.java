package kavita.zocial365.app.datas.http;

import java.io.IOException;
import java.io.InputStream;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import android.util.Log;

public class GetJson {

	public GetJson() {
		// TODO Auto-generated constructor stub
	}

	// Create JSON
	public String getJSON(String URL) {
		String text = null;

		HttpClient httpClient = new DefaultHttpClient();
		Log.i("URL", URL);
		HttpContext localContext = new BasicHttpContext();
		// Log.d("URL", localContext.toString());
		HttpGet httpGet = new HttpGet(URL);

		// Log.d("URL", httpGet.toString());
		try {
			HttpResponse response = httpClient.execute(httpGet, localContext);
			// Log.d("URL", response.toString());
			HttpEntity entity = response.getEntity();
			// Log.d("URL", entity.toString());
			text = getASCIIContentFromEntity(entity);
		} catch (Exception e) {

			Log.d("Exception", e.toString());
			return e.getLocalizedMessage();
		}
		//Log.d("Result", text);
		return text;

	}

	protected String getASCIIContentFromEntity(HttpEntity entity)
			throws IllegalStateException, IOException {
		InputStream in = entity.getContent();
		StringBuffer out = new StringBuffer();
		int n = 1;
		while (n > 0) {
			byte[] b = new byte[4096];
			n = in.read(b);
			if (n > 0)
				out.append(new String(b, 0, n));
		}
		return out.toString();
	}

}