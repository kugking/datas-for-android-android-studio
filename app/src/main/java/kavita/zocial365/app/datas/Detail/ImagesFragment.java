package kavita.zocial365.app.datas.Detail;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.UUID;

import kavita.zocial365.app.datas.DetailActivity;
import kavita.zocial365.app.datas.FullScreenImageActivity;
import kavita.zocial365.app.datas.ImageActivity;
import kavita.zocial365.app.datas.Model.ImagesModel;
import kavita.zocial365.app.datas.MySession;
import kavita.zocial365.app.datas.MyURL;
import kavita.zocial365.app.datas.R;
import kavita.zocial365.app.datas.dao.BaseDao;
import kavita.zocial365.app.datas.http.GetJson;
import kavita.zocial365.app.datas.http.HTTPEngine;
import kavita.zocial365.app.datas.http.HTTPEngineListener;
import kavita.zocial365.app.datas.json.ChangeJson;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ImagesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ImagesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ImagesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private ArrayList<ImagesModel> imagesModels = new ArrayList<ImagesModel>();
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String id_tourist = "";
    private OnFragmentInteractionListener mListener;
    DetailActivity.SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link android.support.v4.view.ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;
    private String limit = "15";
    private ImageListViewAdapter imageListViewAdapter;
    final private int PICK_IMAGE = 1;
    final private int CAPTURE_IMAGE = 2;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ImagesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ImagesFragment newInstance(String param1, String param2) {
        ImagesFragment fragment = new ImagesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public ImagesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        id_tourist = mParam1;
        mViewPager = ((DetailActivity) getActivity()).mViewPager;
        mSectionsPagerAdapter = ((DetailActivity) getActivity()).mSectionsPagerAdapter;
        imagesModels = ((DetailActivity) getActivity()).touristModel.getImagesModels();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = (View) inflater.inflate(R.layout.fragment_images, container, false);
        ListView listView = (ListView) view.findViewById(R.id.listView2);
        imageListViewAdapter = new ImageListViewAdapter(getActivity());
        listView.setAdapter(imageListViewAdapter);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageView_takePhoto);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        getActivity());
                builder.setTitle("Upload Picture");
                builder.setItems(new String[]{"Camera", "Gallery"},
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {

                                if (which == 0) {
                                    // select camera
                                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    startActivityForResult(intent, CAPTURE_IMAGE);

                                } else {
                                    // select gallery
                                    Intent intent = new Intent(Intent.ACTION_PICK);
                                    intent.setType("image/*");
                                    startActivityForResult(intent, PICK_IMAGE);
                                }
                            }

                        });

                builder.show();
//                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//                intent.setType("image/*");
//
//                startActivityForResult(intent, 1);

                // FragmentDetail fragment_detail = new
                // FragmentDetail();
                // Bundle bundle = new Bundle();
                // bundle.putString("id", _arrayListPDF.get(index)
                // .get_id());
                // bundle.putInt("current_year",
                // session.getCurrent_year());
                // fragment_detail.setArguments(bundle);
                // getFragmentManager().beginTransaction()
                // .replace(R.id.container, fragment_detail)
                // .addToBackStack(null).commit();
                // getFragmentManager().beginTransaction().replace(R.id.container, new ImageFragment()).addToBackStack(null).commit();
            }
        });
        // Inflate the layout for this fragment

        // new DownloadImage(id_tourist).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


        return view;
    }

    public String saveBitmap(Bitmap bm) {
        try {
            String uuid = UUID.randomUUID().toString();
            String mBaseFolderPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Dasta/images/";
            File file = new File(mBaseFolderPath);
            file.mkdirs();
            String mFilePath = mBaseFolderPath + "dasta-" + uuid + ".jpg";
            FileOutputStream stream = new FileOutputStream(mFilePath);
            bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            stream.flush();
            stream.close();
            return mFilePath;
        } catch (Exception e) {
            Log.e("Could not save", e.toString());

        }
        return null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == PICK_IMAGE) {
                Uri uri = data.getData();
                Intent intent = new Intent(getActivity(), ImageActivity.class);
                //intent.setData(uri);
                intent.putExtra("url", getRealPathFromURI(uri));
                intent.putExtra("tour_id", ((DetailActivity) getActivity()).tour_id);
                startActivityForResult(intent, 3);
            } else if (requestCode == CAPTURE_IMAGE) {
                Log.i("data uri file", data.getExtras().get("data").toString());
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                String url = saveBitmap(bitmap);
                //  Uri uri = Uri.parse(url);
                Intent intent = new Intent(getActivity(), ImageActivity.class);
                //intent.setData(uri);
                intent.putExtra("url", url);
                intent.putExtra("tour_id", ((DetailActivity) getActivity()).tour_id);
                startActivityForResult(intent, 3);
            }

//
//
//            File file = new File(data.getDataString());

            // Bitmap photo = (Bitmap)data.getExtras().get("data");


//            mSectionsPagerAdapter.add(ImageFragment.newInstance(ImagesFragment.class.getSimpleName(), getRealPathFromURI(uri)));
            //     mSectionsPagerAdapter.notifyDataSetChanged();
//            mViewPager.setCurrentItem(2);

        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    public String getRealPathFromURI(Uri uri) {
        //  Log.i("URI Image", uri.getPath());
        Cursor cursor = getActivity().getContentResolver().query(uri, null,
                null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        //  Log.i("Real path", cursor.getString(idx));
        return cursor.getString(idx);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        new DownloadDetail().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        //((DetailActivity) getActivity()).mSectionsPagerAdapter.notifyDataSetChanged();

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    public class ImageListViewAdapter extends BaseAdapter {
        Activity activity;


        public ImageListViewAdapter(Activity activity) {
            this.activity = activity;

        }

        @Override
        public int getCount() {
            return imagesModels.size();
        }

        @Override
        public Object getItem(int position) {
            return imagesModels.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            Holder holder = null;
            View view = convertView;
            if (convertView != null) {
                holder = (Holder) view.getTag();
            } else {
                view = LayoutInflater.from(activity).inflate(R.layout.layout_image_item, parent, false);
                holder = new Holder();
                holder.imageView = (ImageView) view.findViewById(R.id.imageView6);
                holder.textView_fullscreen = (TextView) view.findViewById(R.id.textView_fullscreen);
                holder.imageViewDiscard = (ImageView) view.findViewById(R.id.button2);
                view.setTag(holder);
            }
            holder.textView_fullscreen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), FullScreenImageActivity.class);
                    intent.putExtra("url",imagesModels.get(position).getImage_url());
                    startActivity(intent);
                }
            });
            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();

            imageLoader.displayImage(imagesModels.get(position).getImage_url(), holder.imageView);

            holder.imageViewDiscard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Delete entry")
                            .setMessage("Are you sure you want to delete this entry?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    deleteImage(position);
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();

                }
            });
            return view;
        }

    }

    public void deleteImage(int position) {
        HTTPEngine.getInstance().deleteImage(new HTTPEngineListener<BaseDao>() {
            @Override
            public void onResponse(BaseDao data, String rawData) {
                Log.i("onResponse", rawData + "");
                new DownloadDetail().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                // imageListViewAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(BaseDao data, String rawData) {
                Log.i("onFailure", rawData + "");
            }
        }, MyURL.HOST_NAME + "tbl_tourist_images/" + imagesModels.get(position).getId() + ".json?auth_token=" + MySession.getInstance().getUserModel().getAuth_token(), getActivity());

    }

    class Holder {
        ImageView imageView;
        // ImageView button;
        TextView textView_fullscreen;
        ImageView imageViewDiscard;
    }

    class DeleteImage extends AsyncTask<Void, Void, Boolean> {

        /**
         * Override this method to perform a computation on a background thread. The
         * specified parameters are the parameters passed to {@link #execute}
         * by the caller of this task.
         * <p/>
         * This method can call {@link #publishProgress} to publish updates
         * on the UI thread.
         *
         * @param params The parameters of the task.
         * @return A result, defined by the subclass of this task.
         * @see #onPreExecute()
         * @see #onPostExecute
         * @see #publishProgress
         */
        @Override
        protected Boolean doInBackground(Void... params) {
            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
        }
    }

    class DownloadImage extends AsyncTask<Void, Void, Boolean> {
        String img_id = "";


        DownloadImage(String img_id) {
            this.img_id = img_id;
            imagesModels.clear();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            GetJson getJson = new GetJson();
            String url = MyURL.HOST_NAME + "tbl_tourist_images.json?q[tour_id_eq]=" + img_id +
                    "&limit=" + limit + "&auth_token="
                    + MySession.getInstance().getUserModel().getAuth_token();
            //   Log.i("Image URL", "url :" + url);
            String result = getJson.getJSON(url);

            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.optString("status", "").equalsIgnoreCase("9200")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("objects");

                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                        ImagesModel imagesModel = new ImagesModel();
                        imagesModel.setId(jsonObject1.optString("id"));
                        imagesModel.setImg_id(jsonObject1.optString("img_id"));
                        imagesModel.setPath(jsonObject1.optString("path"));
                        imagesModel.setTour_id(jsonObject1.optString("tour_id"));
                        imagesModels.add(imagesModel);

                    }

                    return true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return true;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);

        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean) {

                imageListViewAdapter.notifyDataSetChanged();
            }

        }
    }

    class ImageLoader extends AsyncTask<Void, Void, Boolean> {
        Bitmap bitmap;
        String url;
        ImageView imageView;
        com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();

        ImageLoader(ImageView imageView, String url) {
            this.imageView = imageView;
            this.url = url;

        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                //   Log.i("URL ImageLoader", "ImageLoader : " + url);
                bitmap = imageLoader.loadImageSync(url);
                return true;
            } catch (Exception e) {
                e.printStackTrace();

            }
            return false;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);

        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean) {
                imageView.setImageBitmap(bitmap);
                // imageListViewAdapter.notifyDataSetChanged();
                // mSectionsPagerAdapter.notifyDataSetChanged();

            }
        }
    }

    class DownloadDetail extends AsyncTask<Void, Void, Boolean> {
        ProgressDialog progressDialog;

        DownloadDetail() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setTitle("Downloading...");
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            String url = MyURL.HOST_NAME + "tbl_tourist_ups/" + ((DetailActivity) getActivity()).tour_id
                    + ".json?auth_token=" + MySession.getInstance().getUserModel().getAuth_token();
            //     Log.i("URL Detail", "Detail url:" + url);
            GetJson getJson = new GetJson();
            String result = getJson.getJSON(url);
            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.optString("status").equalsIgnoreCase("9200")) {
                    JSONObject jsonObject1 = jsonObject.optJSONObject("objects");
                    ChangeJson changeJson = new ChangeJson();
                    ((DetailActivity) getActivity()).touristModel = changeJson.addTouristModel(jsonObject1);

                    return true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (aBoolean) {
                //mSectionsPagerAdapter.notifyDataSetChanged();
                imagesModels = ((DetailActivity) getActivity()).touristModel.getImagesModels();
                imageListViewAdapter.notifyDataSetChanged();
                // new DownloadImage(id, 0, imageView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                //new DownloadImage(id, 0, imageView2).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
            super.onPostExecute(aBoolean);
        }
    }

}
