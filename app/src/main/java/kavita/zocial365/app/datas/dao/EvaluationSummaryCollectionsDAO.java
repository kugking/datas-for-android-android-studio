package kavita.zocial365.app.datas.dao;

/**
 * Created by kugking on 1/12/15 AD.
 */
public class EvaluationSummaryCollectionsDAO extends BaseDao{
    @SuppressWarnings("objects") private EvaluationSummaryTblDAO objects;

    public EvaluationSummaryTblDAO getObjects() {
        return objects;
    }

    public void setObjects(EvaluationSummaryTblDAO objects) {
        this.objects = objects;
    }
}
