package kavita.zocial365.app.datas.Detail;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kavita.zocial365.app.datas.DetailActivity;
import kavita.zocial365.app.datas.Model.EvaluateModel;
import kavita.zocial365.app.datas.MySession;
import kavita.zocial365.app.datas.MyURL;
import kavita.zocial365.app.datas.R;
import kavita.zocial365.app.datas.http.GetJson;
import kavita.zocial365.app.datas.http.PostJSON;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EvaluateFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EvaluateFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EvaluateFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    ArrayList<EvaluateModel> evaluateModels = new ArrayList<EvaluateModel>();
    ArrayList<EvaluateModel> subevaluateModels = new ArrayList<EvaluateModel>();
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private OnFragmentInteractionListener mListener;
    private ImageListViewAdapter imageListViewAdapter;
    private JSONObject jsonArray = new JSONObject();
    private ArrayList<EvaluateModel> arrayList_Checked_server = new ArrayList<EvaluateModel>();
    private Spinner spinner;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EvaluateFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EvaluateFragment newInstance(String param1, String param2) {
        EvaluateFragment fragment = new EvaluateFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public EvaluateFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        new DownloadTitle().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        new loadChecked().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_evaluate, container, false);
        ListView listView = (ListView) view.findViewById(R.id.listView6);
        imageListViewAdapter = new ImageListViewAdapter(getActivity());
        listView.setAdapter(imageListViewAdapter);

        Button button = (Button) view.findViewById(R.id.button_save);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("JSON Array", jsonArray.toString());
                new SendCheck().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });

        spinner = (Spinner) view.findViewById(R.id.spinner);
        ArrayList<HashMap<String, String>> hashMapArrayList = new ArrayList<HashMap<String, String>>();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        String currentDateandTime = sdf.format(new Date());

        for (int i = 0; i < 5; i++) {
            HashMap<String, String> years_list = new HashMap<String, String>();
            years_list.put("year", (Integer.valueOf(currentDateandTime) - i) + "");
            hashMapArrayList.add(years_list);
        }
        SimpleAdapter dataAdapter = new SimpleAdapter(
                getActivity(), hashMapArrayList,
                R.layout.layout_spinner,
                new String[]{"year"},
                new int[]{R.id.textviewsprinner});
        spinner.setAdapter(dataAdapter);
        spinner.setSelected(true);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                new DownloadTitle().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                new loadChecked().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        return view;
    }


    class SendCheck extends AsyncTask<Void, Void, Boolean> {
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        String url = MyURL.HOST_NAME + "tbl_tourist_evas.json?limit=10000&auth_token="
                + MySession.getInstance().getUserModel().getAuth_token();
        private ProgressDialog progressDialog;

        SendCheck() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setTitle("Downloading...");
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            PostJSON postJSON = new PostJSON();
            nameValuePairs.add(new BasicNameValuePair("year", ((HashMap<String, String>) spinner.getSelectedItem()).get("year")));
            nameValuePairs.add(new BasicNameValuePair("evas_json", jsonArray.toString()));
            nameValuePairs.add(new BasicNameValuePair("auth_token", "MF4hMmOswtglZxHM4sa9jA"));
            nameValuePairs.add(new BasicNameValuePair("tour_id", ((DetailActivity) getActivity()).tour_id));
            String result = postJSON.postJSON(url, nameValuePairs);
            Log.i("Result", result);
            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.optString("status").equalsIgnoreCase("9200")) {
                    return true;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (aBoolean) {
                Intent intent = new Intent(getActivity(), DetailActivity.class);
                intent.putExtra("id", ((DetailActivity) getActivity()).id);
                intent.putExtra("title", ((DetailActivity) getActivity()).title);
                intent.putExtra("tour_id", ((DetailActivity) getActivity()).tour_id);
                startActivity(intent);
                getActivity().finish();
            } else {
                Toast.makeText(getActivity(), "unsuccess", Toast.LENGTH_LONG).show();
            }
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    class DownloadTitle extends AsyncTask<Void, Void, Boolean> {
        String id = "id", sequen_no = "sequen_no", no = "no", description = "description", parent_id = "parent_id",
                total_score = "total_score", have_choice = "have_choice", choice_type = "choice_type",
                remark = "remark",
                rules = "rules",
                lastchild = "lastchild",
                year = "year", topic_general = "topic_general", topic_general_id = "topic_general_id", choice_val = "choice_val";

        DownloadTitle() {

            Log.i("DownloadTitle", "DownloadTitle");
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            GetJson getJson = new GetJson();
            evaluateModels.clear();
            String url = MyURL.HOST_NAME + "topic_generals/tree.json?year=" + ((HashMap<String, String>) spinner.getSelectedItem()).get("year") + "&limit=10000&auth_token="
                    + MySession.getInstance().getUserModel().getAuth_token();
            //  Log.i("Image URL", "url :" + url);
            String result = getJson.getJSON(url);
            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.optString("status", "").equalsIgnoreCase("9200")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("objects");

                    for (int x = 0; x < jsonArray.length(); x++) {
                        JSONObject jsonObject1 = jsonArray.optJSONObject(x);
                        EvaluateModel evaluateModel = new EvaluateModel();
                        evaluateModel.setId(jsonObject1.optString(id));
                        evaluateModel.setSequen_no(jsonObject1.optString(sequen_no));
                        evaluateModel.setNo(jsonObject1.optString(no));
                        evaluateModel.setDescription(jsonObject1.optString(description));
                        evaluateModel.setParent_id(jsonObject1.optString(parent_id));
                        evaluateModel.setTotal_score(jsonObject1.optString(total_score));
                        evaluateModel.setHave_choice(jsonObject1.optString(have_choice));
                        evaluateModel.setChoice_type(jsonObject1.optString(choice_type));
                        evaluateModel.setRemark(jsonObject1.optString(remark));
                        evaluateModel.setRules(jsonObject1.optString(rules));
                        evaluateModel.setLastchild(jsonObject1.optString(lastchild));
                        evaluateModel.setYear(jsonObject1.optString(year));
                        evaluateModel.setTopic_general(jsonObject1.optJSONArray(topic_general));
                        evaluateModel.setTopic_general_id(jsonObject1.optString(topic_general_id, ""));
                        evaluateModel.setChoice_val(jsonObject1.optString(choice_val, ""));
                        //   Log.i("DownloadTitle :", "" + evaluateModel.getNo() + " " + evaluateModel.getDescription());
                        if (evaluateModel.getLastchild().equalsIgnoreCase("false")) {

                            for (int y = 0; y < evaluateModel.getTopic_general().length(); y++) {
                                JSONObject jsonObject2 = evaluateModel.getTopic_general().getJSONObject(y);
                                EvaluateModel evaluateModel1 = new EvaluateModel();
                                evaluateModel1.setId(jsonObject2.optString(id));
                                evaluateModel1.setSequen_no(jsonObject2.optString(sequen_no));
                                evaluateModel1.setNo(jsonObject2.optString(no));
                                evaluateModel1.setDescription(jsonObject2.optString(description));
                                evaluateModel1.setParent_id(jsonObject2.optString(parent_id));
                                evaluateModel1.setTotal_score(jsonObject2.optString(total_score));
                                evaluateModel1.setHave_choice(jsonObject2.optString(have_choice));
                                evaluateModel1.setChoice_type(jsonObject2.optString(choice_type));
                                evaluateModel1.setRemark(jsonObject2.optString(remark));
                                evaluateModel1.setRules(jsonObject2.optString(rules));
                                evaluateModel1.setLastchild(jsonObject2.optString(lastchild));
                                evaluateModel1.setYear(jsonObject2.optString(year));
                                evaluateModel1.setTopic_general(jsonObject2.optJSONArray(topic_general));
                                evaluateModel1.setTopic_general_id(jsonObject2.optString(topic_general_id, ""));
                                evaluateModel1.setChoice_val(jsonObject2.optString(choice_val, ""));
                                //    Log.i("DownloadTitle :", "" + evaluateModel1.getNo() + " " + evaluateModel1.getDescription());
                                if (evaluateModel1.getLastchild().equalsIgnoreCase("false")) {

                                    for (int r = 0; r < evaluateModel1.getTopic_general().length(); r++) {
                                        JSONObject jsonObject3 = evaluateModel1.getTopic_general().getJSONObject(r);
                                        EvaluateModel evaluateModel2 = new EvaluateModel();
                                        evaluateModel2.setId(jsonObject3.optString(id));
                                        evaluateModel2.setSequen_no(jsonObject3.optString(sequen_no));
                                        evaluateModel2.setNo(jsonObject3.optString(no));
                                        evaluateModel2.setDescription(jsonObject3.optString(description));
                                        evaluateModel2.setParent_id(jsonObject3.optString(parent_id));
                                        evaluateModel2.setTotal_score(jsonObject3.optString(total_score));
                                        evaluateModel2.setHave_choice(jsonObject3.optString(have_choice));
                                        evaluateModel2.setChoice_type(jsonObject3.optString(choice_type));
                                        evaluateModel2.setRemark(jsonObject3.optString(remark));
                                        evaluateModel2.setRules(jsonObject3.optString(rules));
                                        evaluateModel2.setLastchild(jsonObject3.optString(lastchild));
                                        evaluateModel2.setYear(jsonObject3.optString(year));
                                        evaluateModel2.setTopic_general(jsonObject3.optJSONArray(topic_general));
                                        evaluateModel2.setTopic_general_id(jsonObject3.optString(topic_general_id, ""));
                                        evaluateModel2.setChoice_val(jsonObject3.optString(choice_val, ""));
                                        Log.i("Evaluate :", "" + evaluateModel2.getNo() + " " + evaluateModel2.getDescription());
                                        evaluateModel1.addSubEvaluate(evaluateModel2);
                                    }

                                }
                                evaluateModel.addSubEvaluate(evaluateModel1);
                            }

                        }
                        evaluateModels.add(evaluateModel);
                    }

                    //  Log.i("Evaluate : size", "" + evaluateModels.size());
                    return true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return true;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);

        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            imageListViewAdapter.notifyDataSetChanged();


        }
    }

    public ArrayList<EvaluateModel> addEvaluateModelandCheck(JSONArray jsonArray) {
        String id = "id", sequen_no = "sequen_no", no = "no", description = "description", parent_id = "parent_id",
                total_score = "total_score", have_choice = "have_choice", choice_type = "choice_type",
                remark = "remark",
                rules = "rules",
                lastchild = "lastchild", choice_val = "choice_val",
                year = "year", topic_general = "topic_general", topic_general_id = "topic_general_id";
        ArrayList<EvaluateModel> arrayList = new ArrayList<EvaluateModel>();

        for (int x = 0; x < jsonArray.length(); x++) {
            EvaluateModel evaluateModel = new EvaluateModel();
            JSONObject jsonObject1 = jsonArray.optJSONObject(x);
            evaluateModel.setId(jsonObject1.optString(id));
            evaluateModel.setSequen_no(jsonObject1.optString(sequen_no));
            evaluateModel.setNo(jsonObject1.optString(no));
            evaluateModel.setDescription(jsonObject1.optString(description));
            evaluateModel.setParent_id(jsonObject1.optString(parent_id));
            evaluateModel.setTotal_score(jsonObject1.optString(total_score));
            evaluateModel.setHave_choice(jsonObject1.optString(have_choice));
            evaluateModel.setChoice_type(jsonObject1.optString(choice_type));
            evaluateModel.setRemark(jsonObject1.optString(remark));
            evaluateModel.setRules(jsonObject1.optString(rules));
            evaluateModel.setLastchild(jsonObject1.optString(lastchild));
            evaluateModel.setYear(jsonObject1.optString(year));
            evaluateModel.setTopic_general(jsonObject1.optJSONArray(topic_general));
            evaluateModel.setTopic_general_id(jsonObject1.optString(topic_general_id, ""));
            evaluateModel.setChoice_val(jsonObject1.optString(choice_val, ""));
            arrayList.add(evaluateModel);
        }

        return arrayList;

    }

    class loadChecked extends AsyncTask<Void, Void, Void> {

        String url = MyURL.HOST_NAME + "tbl_tourist_evas.json?q[tour_id_eq]=" + ((DetailActivity) getActivity()).tour_id
                + "&q[year_eq]=" + ((HashMap<String, String>) spinner.getSelectedItem()).get("year") + "&limit=10000&auth_token=" + MySession.getInstance().getUserModel().getAuth_token();

        loadChecked() {
            arrayList_Checked_server.clear();
            Log.i("loadChecked", "loadChecked");
        }

        @Override
        protected Void doInBackground(Void... params) {
            GetJson getJson = new GetJson();

            String result = getJson.getJSON(url);
            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.optString("status", "").equalsIgnoreCase("9200")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("objects");
                    arrayList_Checked_server = addEvaluateModelandCheck(jsonArray);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            imageListViewAdapter.notifyDataSetChanged();
        }
    }

    public class ImageListViewAdapter extends BaseAdapter {
        Activity activity;
        // ArrayList<HashMap<Integer, View>> arrayList = new ArrayList<HashMap<Integer, View>>();
        HashMap<Integer, View> hashMap = new HashMap<Integer, View>();

        public ImageListViewAdapter(Activity activity) {
            this.activity = activity;
            hashMap.clear();
            //touristModels = new ArrayList<String>();
        }

        @Override
        public int getCount() {
            return evaluateModels.size();
        }

        @Override
        public Object getItem(int position) {
            return evaluateModels.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            Holder holder = null;
            View view = convertView;
            if (convertView != null) {
                holder = (Holder) view.getTag();

            } else {
                holder = new Holder();
                view = LayoutInflater.from(activity).inflate(R.layout.layout_item_evaluate, parent, false);
                holder.textView1 = (TextView) view.findViewById(R.id.textView109);
                holder.linearLayout = (LinearLayout) view.findViewById(R.id.LayoutEvaluation);
                view.setTag(holder);
            }

            holder.linearLayout.removeAllViews();

            holder.textView1.setText(evaluateModels.get(position).getNo().replace(".", "") + "." + evaluateModels.get(position).getDescription());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            if (evaluateModels.get(position).getLastchild().equalsIgnoreCase("false")) {

                for (int i = 0; i < evaluateModels.get(position).getSubEvaluateModels().size(); i++) {
                    Log.i("evaluateModels i", "i=" + i + " ," + evaluateModels.get(position).getSubEvaluateModels().get(i).getNo()
                            + "" + evaluateModels.get(position).getSubEvaluateModels().get(i).getDescription());

                    final int i1 = i;
                    if (evaluateModels.get(position).getSubEvaluateModels().get(i).getLastchild().equalsIgnoreCase("false")) {

                        TextView textView = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.layout_subevaluation, null, false);
                        textView.setText(evaluateModels.get(position).getSubEvaluateModels().get(i).getNo() + "" + evaluateModels
                                .get(position).getSubEvaluateModels().get(i).getDescription());

                        holder.linearLayout.addView(textView, params);

                        for (int x = 0; x < evaluateModels.get(position).getSubEvaluateModels().get(i).getSubEvaluateModels().size(); x++) {
                            final int x1 = x;
                            final RadioGroup radipgroup = (RadioGroup) LayoutInflater.from(getActivity()).inflate(R.layout.layout_subevaluate_item_radio_group, null, false);

                            TextView textView_des = (TextView) radipgroup.findViewById(R.id.textView24);
                            textView_des.setText(evaluateModels.get(position).getSubEvaluateModels().get(i)
                                    .getSubEvaluateModels().get(x).getNo() + "" + evaluateModels.get(position).getSubEvaluateModels()
                                    .get(i).getSubEvaluateModels().get(x).getDescription());

                            Log.i("evaluateModels x", "x=" + x1 + " ," + evaluateModels.get(position).getSubEvaluateModels().get(i)
                                    .getSubEvaluateModels().get(x).getNo() + "" + evaluateModels.get(position).getSubEvaluateModels()
                                    .get(i).getSubEvaluateModels().get(x).getDescription());
                            for (int y = 0; y < arrayList_Checked_server.size(); y++) {
                                if (arrayList_Checked_server.get(y).getTopic_general_id()
                                        .equalsIgnoreCase(evaluateModels.get(position).getSubEvaluateModels().get(i).getSubEvaluateModels().get(x).getId())) {
                                    if (arrayList_Checked_server.get(y).getChoice_val().equalsIgnoreCase("0")) {
                                        radipgroup.check(R.id.radioButton0);
                                    } else if (arrayList_Checked_server.get(y).getChoice_val().equalsIgnoreCase("1")) {
                                        radipgroup.check(R.id.radioButton1);
                                    } else if (arrayList_Checked_server.get(y).getChoice_val().equalsIgnoreCase("2")) {
                                        radipgroup.check(R.id.radioButton2);
                                    }

                                }

                            }
                            for (int y = 0; y < jsonArray.length(); y++) {
                                String index = jsonArray.optString(evaluateModels.get(position).getSubEvaluateModels().get(i1).getSubEvaluateModels().get(x1).getId().toString());
                                if (index.equalsIgnoreCase("0")) {
                                    radipgroup.check(R.id.radioButton0);
                                } else if (index.equalsIgnoreCase("1")) {
                                    radipgroup.check(R.id.radioButton1);
                                } else if (index.equalsIgnoreCase("2")) {
                                    radipgroup.check(R.id.radioButton2);
                                }
                            }

                            switch (radipgroup.getCheckedRadioButtonId()) {
                                case R.id.radioButton0:
                                    try {
                                        RadioButton radioButton = (RadioButton) radipgroup.findViewById(R.id.radioButton0);
                                        jsonArray.put(evaluateModels.get(position).getSubEvaluateModels().get(i1)
                                                .getSubEvaluateModels().get(x1).getId().toString(), radioButton.getText());
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    break;
                                case R.id.radioButton1:
                                    try {
                                        RadioButton radioButton1 = (RadioButton) radipgroup.findViewById(R.id.radioButton1);
                                        jsonArray.put(evaluateModels.get(position).getSubEvaluateModels().get(i1)
                                                .getSubEvaluateModels().get(x1).getId().toString(), radioButton1.getText());
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    break;
                                case R.id.radioButton2:
                                    try {
                                        RadioButton radioButton = (RadioButton) radipgroup.findViewById(R.id.radioButton2);
                                        jsonArray.put(evaluateModels.get(position).getSubEvaluateModels().get(i1)
                                                .getSubEvaluateModels().get(x1).getId().toString(), radioButton.getText());
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    break;
                            }
                            radipgroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(RadioGroup group, int checkedId) {
                                    switch (checkedId) {
                                        case R.id.radioButton0:
                                            try {
                                                RadioButton radioButton = (RadioButton) radipgroup.findViewById(R.id.radioButton0);
                                                jsonArray.put(evaluateModels.get(position).getSubEvaluateModels().get(i1)
                                                        .getSubEvaluateModels().get(x1).getId().toString(), radioButton.getText());
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            break;
                                        case R.id.radioButton1:
                                            try {
                                                RadioButton radioButton1 = (RadioButton) radipgroup.findViewById(R.id.radioButton1);
                                                jsonArray.put(evaluateModels.get(position).getSubEvaluateModels().get(i1)
                                                        .getSubEvaluateModels().get(x1).getId().toString(), radioButton1.getText());
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            break;
                                        case R.id.radioButton2:
                                            try {
                                                RadioButton radioButton = (RadioButton) radipgroup.findViewById(R.id.radioButton2);
                                                jsonArray.put(evaluateModels.get(position).getSubEvaluateModels().get(i1)
                                                        .getSubEvaluateModels().get(x1).getId().toString(), radioButton.getText());
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            break;
                                    }
                                }
                            });
                            holder.linearLayout.addView(radipgroup, params);
                        }

                    } else if (evaluateModels.get(position).getSubEvaluateModels().get(i).getLastchild().equalsIgnoreCase("true")) {

                        final RadioGroup radipgroup = (RadioGroup) LayoutInflater.from(getActivity()).inflate(R.layout.layout_subevaluate_item_radio_group, null, false);

                        TextView textView_des = (TextView) radipgroup.findViewById(R.id.textView24);

                        textView_des.setText(evaluateModels.get(position).getSubEvaluateModels().get(i).getNo() + ""
                                + evaluateModels.get(position).getSubEvaluateModels().get(i).getDescription());

                        for (int y = 0; y < arrayList_Checked_server.size(); y++) {
                            if (arrayList_Checked_server.get(y).getTopic_general_id()
                                    .equalsIgnoreCase(evaluateModels.get(position).getSubEvaluateModels().get(i).getId())) {

                                if (arrayList_Checked_server.get(y).getChoice_val().equalsIgnoreCase("0")) {
                                    radipgroup.check(R.id.radioButton0);
                                } else if (arrayList_Checked_server.get(y).getChoice_val().equalsIgnoreCase("1")) {
                                    radipgroup.check(R.id.radioButton1);
                                } else if (arrayList_Checked_server.get(y).getChoice_val().equalsIgnoreCase("2")) {
                                    radipgroup.check(R.id.radioButton2);
                                }
                            }
                        }
                        for (int y = 0; y < jsonArray.length(); y++) {
                            String index = jsonArray.optString(evaluateModels.get(position).getSubEvaluateModels().get(i1).getId().toString().toString());
                            if (index.equalsIgnoreCase("0")) {
                                radipgroup.check(R.id.radioButton0);
                            } else if (index.equalsIgnoreCase("1")) {
                                radipgroup.check(R.id.radioButton1);
                            } else if (index.equalsIgnoreCase("2")) {
                                radipgroup.check(R.id.radioButton2);
                            }
                        }


                        switch (radipgroup.getCheckedRadioButtonId()) {
                            case R.id.radioButton0:
                                try {
                                    RadioButton radioButton = (RadioButton) radipgroup.findViewById(R.id.radioButton0);
                                    jsonArray.put(evaluateModels.get(position).getSubEvaluateModels().get(i1).getId().toString(), radioButton.getText());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                break;
                            case R.id.radioButton1:
                                try {
                                    RadioButton radioButton1 = (RadioButton) radipgroup.findViewById(R.id.radioButton1);
                                    jsonArray.put(evaluateModels.get(position).getSubEvaluateModels().get(i1).getId().toString(), radioButton1.getText());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                break;
                            case R.id.radioButton2:
                                try {
                                    RadioButton radioButton = (RadioButton) radipgroup.findViewById(R.id.radioButton2);
                                    jsonArray.put(evaluateModels.get(position).getSubEvaluateModels().get(i1).getId().toString(), radioButton.getText());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                break;
                        }

                        radipgroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup group, int checkedId) {
                                switch (checkedId) {
                                    case R.id.radioButton0:
                                        try {
                                            RadioButton radioButton = (RadioButton) radipgroup.findViewById(R.id.radioButton0);
                                            jsonArray.put(evaluateModels.get(position).getSubEvaluateModels().get(i1).getId().toString(), radioButton.getText());
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        break;
                                    case R.id.radioButton1:
                                        try {
                                            RadioButton radioButton1 = (RadioButton) radipgroup.findViewById(R.id.radioButton1);
                                            jsonArray.put(evaluateModels.get(position).getSubEvaluateModels().get(i1).getId().toString(), radioButton1.getText());
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        break;
                                    case R.id.radioButton2:
                                        try {
                                            RadioButton radioButton = (RadioButton) radipgroup.findViewById(R.id.radioButton2);
                                            jsonArray.put(evaluateModels.get(position).getSubEvaluateModels().get(i1).getId().toString(), radioButton.getText());
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        break;
                                }
                            }
                        });
                        holder.linearLayout.addView(radipgroup, params);
                    }

                }


                // hashMap.put(position, view);

            }


            return view;
        }
    }

    class Radiogroup {
        RadioGroup radioGroup;

    }

    class Holder {
        TextView textView1;
        LinearLayout linearLayout;
    }
}
