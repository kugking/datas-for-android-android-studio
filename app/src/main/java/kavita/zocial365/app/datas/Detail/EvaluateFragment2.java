package kavita.zocial365.app.datas.Detail;

import android.app.Activity;
import android.app.Fragment;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import kavita.zocial365.app.datas.DetailActivity;
import kavita.zocial365.app.datas.Model.EvaluateModel;
import kavita.zocial365.app.datas.MySession;
import kavita.zocial365.app.datas.MyURL;
import kavita.zocial365.app.datas.R;
import kavita.zocial365.app.datas.http.GetJson;
import kavita.zocial365.app.datas.http.PostJSON;


/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link kavita.zocial365.app.datas.Detail.EvaluateFragment2.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link kavita.zocial365.app.datas.Detail.EvaluateFragment2#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EvaluateFragment2 extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    ArrayList<EvaluateModel> evaluateModels = new ArrayList<EvaluateModel>();
    ArrayList<EvaluateModel> subevaluateModels = new ArrayList<EvaluateModel>();
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    String year = "2014";
    private OnFragmentInteractionListener mListener;
    private ImageListViewAdapter imageListViewAdapter;
    private JSONObject jsonArray = new JSONObject();

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EvaluateFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EvaluateFragment2 newInstance(String param1, String param2) {
        EvaluateFragment2 fragment = new EvaluateFragment2();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public EvaluateFragment2() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_evaluate, container, false);
        ListView listView = (ListView) view.findViewById(R.id.listView6);
        imageListViewAdapter = new ImageListViewAdapter(getActivity());
        listView.setAdapter(imageListViewAdapter);
        new DownloadTitle().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        Button button = (Button) view.findViewById(R.id.button_save);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("JSON Array", jsonArray.toString());
                new SendCheck().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });
        return view;
    }



    class SendCheck extends AsyncTask<Void, Void, Boolean> {
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        String url = MyURL.HOST_NAME + "tbl_tourist_evas.json?auth_token="
                + MySession.getInstance().getUserModel().getAuth_token();

        SendCheck() {

        }

        @Override
        protected Boolean doInBackground(Void... params) {
            PostJSON postJSON = new PostJSON();
            nameValuePairs.add(new BasicNameValuePair("year", year));
            nameValuePairs.add(new BasicNameValuePair("evas_json", jsonArray.toString()));
            nameValuePairs.add(new BasicNameValuePair("auth_token", "MF4hMmOswtglZxHM4sa9jA"));
            nameValuePairs.add(new BasicNameValuePair("tour_id", ((DetailActivity) getActivity()).tour_id));
            String result = postJSON.postJSON(url, nameValuePairs);
            Log.i("Result", result);
            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    class DownloadTitle extends AsyncTask<Void, Void, Boolean> {
        String id = "id", sequen_no = "sequen_no", no = "no", description = "description", parent_id = "parent_id",
                total_score = "total_score", have_choice = "have_choice", choice_type = "choice_type",
                remark = "remark",
                rules = "rules",
                lastchild = "lastchild",
                year = "year", topic_general = "topic_general";

        DownloadTitle() {

        }

        @Override
        protected Boolean doInBackground(Void... params) {
            GetJson getJson = new GetJson();
            String url = MyURL.HOST_NAME + "topic_generals/tree.json?year=2013&auth_token="
                    + MySession.getInstance().getUserModel().getAuth_token();
          //  Log.i("Image URL", "url :" + url);
            String result = getJson.getJSON(url);
            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.optString("status", "").equalsIgnoreCase("9200")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("objects");

                    for (int x = 0; x < jsonArray.length(); x++) {
                        JSONObject jsonObject1 = jsonArray.optJSONObject(x);
                        EvaluateModel evaluateModel = new EvaluateModel();
                        evaluateModel.setId(jsonObject1.optString(id));
                        evaluateModel.setSequen_no(jsonObject1.optString(sequen_no));
                        evaluateModel.setNo(jsonObject1.optString(no));
                        evaluateModel.setDescription(jsonObject1.optString(description));
                        evaluateModel.setParent_id(jsonObject1.optString(parent_id));
                        evaluateModel.setTotal_score(jsonObject1.optString(total_score));
                        evaluateModel.setHave_choice(jsonObject1.optString(have_choice));
                        evaluateModel.setChoice_type(jsonObject1.optString(choice_type));
                        evaluateModel.setRemark(jsonObject1.optString(remark));
                        evaluateModel.setRules(jsonObject1.optString(rules));
                        evaluateModel.setLastchild(jsonObject1.optString(lastchild));
                        evaluateModel.setYear(jsonObject1.optString(year));
                        evaluateModel.setTopic_general(jsonObject1.optJSONArray(topic_general));
                      //  Log.i("Evaluate :", "" + evaluateModel.getNo() + " " + evaluateModel.getDescription());
                        if (evaluateModel.getLastchild().equalsIgnoreCase("false")) {

                            for (int y = 0; y < evaluateModel.getTopic_general().length(); y++) {
                                JSONObject jsonObject2 = evaluateModel.getTopic_general().getJSONObject(y);
                                EvaluateModel evaluateModel1 = new EvaluateModel();
                                evaluateModel1.setId(jsonObject2.optString(id));
                                evaluateModel1.setSequen_no(jsonObject2.optString(sequen_no));
                                evaluateModel1.setNo(jsonObject2.optString(no));
                                evaluateModel1.setDescription(jsonObject2.optString(description));
                                evaluateModel1.setParent_id(jsonObject2.optString(parent_id));
                                evaluateModel1.setTotal_score(jsonObject2.optString(total_score));
                                evaluateModel1.setHave_choice(jsonObject2.optString(have_choice));
                                evaluateModel1.setChoice_type(jsonObject2.optString(choice_type));
                                evaluateModel1.setRemark(jsonObject2.optString(remark));
                                evaluateModel1.setRules(jsonObject2.optString(rules));
                                evaluateModel1.setLastchild(jsonObject2.optString(lastchild));
                                evaluateModel1.setYear(jsonObject2.optString(year));
                                evaluateModel1.setTopic_general(jsonObject2.optJSONArray(topic_general));
                              //  Log.i("Evaluate :", "" + evaluateModel1.getNo() + " " + evaluateModel1.getDescription());
                                if (evaluateModel1.getLastchild().equalsIgnoreCase("false")) {

                                    for (int r = 0; r < evaluateModel1.getTopic_general().length(); r++) {
                                        JSONObject jsonObject3 = evaluateModel1.getTopic_general().getJSONObject(r);
                                        EvaluateModel evaluateModel2 = new EvaluateModel();
                                        evaluateModel2.setId(jsonObject3.optString(id));
                                        evaluateModel2.setSequen_no(jsonObject3.optString(sequen_no));
                                        evaluateModel2.setNo(jsonObject3.optString(no));
                                        evaluateModel2.setDescription(jsonObject3.optString(description));
                                        evaluateModel2.setParent_id(jsonObject3.optString(parent_id));
                                        evaluateModel2.setTotal_score(jsonObject3.optString(total_score));
                                        evaluateModel2.setHave_choice(jsonObject3.optString(have_choice));
                                        evaluateModel2.setChoice_type(jsonObject3.optString(choice_type));
                                        evaluateModel2.setRemark(jsonObject3.optString(remark));
                                        evaluateModel2.setRules(jsonObject3.optString(rules));
                                        evaluateModel2.setLastchild(jsonObject3.optString(lastchild));
                                        evaluateModel2.setYear(jsonObject3.optString(year));
                                        evaluateModel2.setTopic_general(jsonObject3.optJSONArray(topic_general));

                                      //  Log.i("Evaluate :", "" + evaluateModel2.getNo() + " " + evaluateModel2.getDescription());
                                        evaluateModel1.addSubEvaluate(evaluateModel2);
                                    }

                                }
                                evaluateModel.addSubEvaluate(evaluateModel1);
                            }

                        }
                        evaluateModels.add(evaluateModel);
                    }

                  //  Log.i("Evaluate : size", "" + evaluateModels.size());
                    return true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return true;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);

        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean) {
                imageListViewAdapter.notifyDataSetChanged();
            }

        }
    }

    public EvaluateModel addEvaluateModelandCheck(JSONArray jsonArray) {
        String id = "id", sequen_no = "sequen_no", no = "no", description = "description", parent_id = "parent_id",
                total_score = "total_score", have_choice = "have_choice", choice_type = "choice_type",
                remark = "remark",
                rules = "rules",
                lastchild = "lastchild",
                year = "year", topic_general = "topic_general";
        EvaluateModel evaluateModel = new EvaluateModel();
        for (int x = 0; x < jsonArray.length(); x++) {
            JSONObject jsonObject1 = jsonArray.optJSONObject(x);

            evaluateModel.setId(jsonObject1.optString(id));
            evaluateModel.setSequen_no(jsonObject1.optString(sequen_no));
            evaluateModel.setNo(jsonObject1.optString(no));
            evaluateModel.setDescription(jsonObject1.optString(description));
            evaluateModel.setParent_id(jsonObject1.optString(parent_id));
            evaluateModel.setTotal_score(jsonObject1.optString(total_score));
            evaluateModel.setHave_choice(jsonObject1.optString(have_choice));
            evaluateModel.setChoice_type(jsonObject1.optString(choice_type));
            evaluateModel.setRemark(jsonObject1.optString(remark));
            evaluateModel.setRules(jsonObject1.optString(rules));
            evaluateModel.setLastchild(jsonObject1.optString(lastchild));
            evaluateModel.setYear(jsonObject1.optString(year));
            evaluateModel.setTopic_general(jsonObject1.optJSONArray(topic_general));
        }
        return evaluateModel;

    }

    public class ImageListViewAdapter extends BaseAdapter {
        Activity activity;


        public ImageListViewAdapter(Activity activity) {
            this.activity = activity;
            //touristModels = new ArrayList<String>();
        }

        @Override
        public int getCount() {
            return evaluateModels.size();
        }

        @Override
        public Object getItem(int position) {
            return evaluateModels.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            Holder holder = null;
            View view = convertView;

            if (convertView == null) {
                holder = new Holder();
                view = LayoutInflater.from(activity).inflate(R.layout.layout_item_evaluate, parent, false);
                holder.textView1 = (TextView) view.findViewById(R.id.textView109);
                holder.linearLayout = (LinearLayout) view.findViewById(R.id.LayoutEvaluation);
                view.setTag(holder);
            } else {
                holder = (Holder) view.getTag();
            }

            holder.linearLayout.removeAllViews();
            holder.textView1.setText(evaluateModels.get(position).getNo() + "." + evaluateModels.get(position).getDescription());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            if (evaluateModels.get(position).getLastchild().equalsIgnoreCase("false")) {

                for (int i = 0; i < evaluateModels.get(position).getSubEvaluateModels().size(); i++) {
                    final int i1 = i;
                    if (evaluateModels.get(position).getSubEvaluateModels().get(i).getLastchild().equalsIgnoreCase("false")) {
                        TextView textView = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.layout_subevaluation, null, false);
                        textView.setText(evaluateModels.get(position).getSubEvaluateModels().get(i).getNo() + "" + evaluateModels.get(position).getSubEvaluateModels().get(i).getDescription());
                        holder.linearLayout.addView(textView, params);
                        for (int x = 0; x < evaluateModels.get(position).getSubEvaluateModels().get(i).getSubEvaluateModels().size(); x++) {
                            final int x1 = x;
                            Switch aSwitch = (Switch) LayoutInflater.from(getActivity()).inflate(R.layout.layout_subevaluate_item, null, false);
                            aSwitch.setChecked(evaluateModels.get(position).getSubEvaluateModels().get(i1).getSubEvaluateModels().get(x1).isChecked());
                            aSwitch.setText(evaluateModels.get(position).getSubEvaluateModels().get(i)
                                    .getSubEvaluateModels().get(x).getNo() + "" + evaluateModels.get(position).getSubEvaluateModels().get(i).getSubEvaluateModels().get(x).getDescription());
                            aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    evaluateModels.get(position).getSubEvaluateModels().get(i1).getSubEvaluateModels().get(x1).setChecked(isChecked);
                                    if (isChecked) {
                                        try {

                                            jsonArray.put(evaluateModels.get(position).getSubEvaluateModels().get(i1).getSubEvaluateModels().get(x1).getId().toString(), "1");
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                      //  Log.i("TOP_id 1", evaluateModels.get(position).getSubEvaluateModels().get(i1).getSubEvaluateModels().get(x1).getId() + "");
                                    } else {
                                        try {
                                            jsonArray.put(evaluateModels.get(position).getSubEvaluateModels().get(i1).getSubEvaluateModels().get(x1).getId().toString(), "0");
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                       // Log.i("TOP_id 1", evaluateModels.get(position).getSubEvaluateModels().get(i1).getSubEvaluateModels().get(x1).getId() + "");
                                    }
                                }
                            });
                            holder.linearLayout.addView(aSwitch, params);
                        }
                    } else if (evaluateModels.get(position).getSubEvaluateModels().get(i).getLastchild().equalsIgnoreCase("true")) {
                        Switch aSwitch = (Switch) LayoutInflater.from(getActivity()).inflate(R.layout.layout_subevaluate_item, null, false);
                        aSwitch.setChecked(evaluateModels.get(position).getSubEvaluateModels().get(i).isChecked());
                        aSwitch.setText(evaluateModels.get(position).getSubEvaluateModels().get(i).getNo() + "" + evaluateModels.get(position).getSubEvaluateModels().get(i).getDescription());

                        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                evaluateModels.get(position).getSubEvaluateModels().get(i1).setChecked(isChecked);
                                if (isChecked) {
                                    try {
                                        jsonArray.put(evaluateModels.get(position).getSubEvaluateModels().get(i1).getId().toString(), "1");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                   // Log.i("TOP_id 1", String.valueOf(evaluateModels.get(position).getSubEvaluateModels().get(i1).getId()));
                                } else {
                                    try {
                                        jsonArray.put(evaluateModels.get(position).getSubEvaluateModels().get(i1).getId().toString(), "0");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                  //  Log.i("TOP_id 1", String.valueOf(evaluateModels.get(position).getSubEvaluateModels().get(i1).getId()));
                                }

                            }
                        });
                        holder.linearLayout.addView(aSwitch, params);
                    }

                }


            }


            return view;
        }
    }


    class Holder {
        TextView textView1;
        LinearLayout linearLayout;
    }
}
