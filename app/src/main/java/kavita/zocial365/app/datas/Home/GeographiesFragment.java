package kavita.zocial365.app.datas.Home;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import kavita.zocial365.app.datas.HomeActivity;
import kavita.zocial365.app.datas.Model.AttractionTypeModel;
import kavita.zocial365.app.datas.Model.GeographiesModel;
import kavita.zocial365.app.datas.MySession;
import kavita.zocial365.app.datas.MyURL;
import kavita.zocial365.app.datas.R;
import kavita.zocial365.app.datas.http.GetJson;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link GeographiesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link GeographiesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GeographiesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    ArrayList<GeographiesModel> geographiesModels = new ArrayList<GeographiesModel>();
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    ListView listView;
    private GeographiesAdapter geographiesAdapter;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment GeographiesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GeographiesFragment newInstance(String param1, String param2) {
        GeographiesFragment fragment = new GeographiesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public GeographiesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_geographies, container, false);
        listView = (ListView) view.findViewById(R.id.listView);
        geographiesAdapter = new GeographiesAdapter();
        listView.setAdapter(geographiesAdapter);
        new DownloadData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ((HomeActivity) getActivity()).geographies = geographiesModels.get(position).getGeo_name();
                ((HomeActivity) getActivity()).geographies_id = geographiesModels.get(position).getGeo_id();
                ((HomeActivity) getActivity()).mViewPager.setCurrentItem(2);

            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    class GeographiesAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return geographiesModels.size();
        }

        @Override
        public Object getItem(int position) {
            return geographiesModels.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = null;
            view = LayoutInflater.from(getActivity()).inflate(R.layout.layout_attraction_type, null, false);
            TextView textView1 = (TextView) view.findViewById(R.id.textViewTitle);
            TextView textView2 = (TextView) view.findViewById(R.id.textView7);
            textView1.setText(geographiesModels.get(position).getGeo_name());
            textView2.setText("");
            return view;
        }
    }

    class DownloadData extends AsyncTask<Void, Void, Boolean> {
        String id = "id", geo_id = "geo_id", geo_name = "geo_name";
        GetJson getJson = new GetJson();
        String url = MyURL.HOST_NAME + "location_geographies.json?auth_token=" + MySession.getInstance().getUserModel().getAuth_token();
        public static final String OBJECTS = "objects";
        public static final String S9200 = "9200";
        public static final String STATUS = "status";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            geographiesModels.clear();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            String result = getJson.getJSON(url);
            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.optString(STATUS).equalsIgnoreCase(S9200)) {
                    JSONArray jsonArray = jsonObject.optJSONArray(OBJECTS);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                        GeographiesModel geographiesModel = new GeographiesModel();
                        geographiesModel.setId(jsonObject1.optString(id));
                        geographiesModel.setGeo_id(jsonObject1.optString(geo_id));
                        geographiesModel.setGeo_name(jsonObject1.optString(geo_name));
                        geographiesModels.add(geographiesModel);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            geographiesAdapter.notifyDataSetChanged();
        }
    }
}
