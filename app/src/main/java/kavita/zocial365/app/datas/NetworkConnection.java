/*
 * Created by Kavita Somana at 365Zocial.com on 2015.
 */

package kavita.zocial365.app.datas;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by kugking on 2/3/15 AD.
 */
public class NetworkConnection {
    public static boolean isNetworkAvailable(final Activity activity) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        if ((activeNetworkInfo != null && activeNetworkInfo.isConnected()) == false) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    MyAlertDialog.mAlertDialog(activity, "Not connect internet.", "Internet Connection");
                }
            });
        }
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

//    public static boolean isNetworkAvailable() {
//        ConnectivityManager connectivityManager
//                = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
//        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
//    }
}
