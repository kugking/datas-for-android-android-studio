package kavita.zocial365.app.datas;

import android.app.Activity;
import android.app.AlertDialog;

/**
 * Created by kugking on 3/14/15 AD.
 */
public class MyAlertDialog {
    public static void mAlertDialog(final Activity activity, final String text, final String title) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity).setTitle(title).setMessage(text).setIcon(android.R.drawable.stat_sys_warning);
                builder.show();
            }
        });

    }

    public static void alert(final Activity activity, final String title) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity).setTitle(title).setIcon(android.R.drawable.stat_sys_warning);
                builder.show();
            }
        });

    }
}
