package kavita.zocial365.app.datas.Detail;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.StringTokenizer;

import kavita.zocial365.app.datas.DetailActivity;
import kavita.zocial365.app.datas.GPS.GPSTracker;
import kavita.zocial365.app.datas.Model.TouristModel;
import kavita.zocial365.app.datas.MySession;
import kavita.zocial365.app.datas.MyURL;
import kavita.zocial365.app.datas.R;
import kavita.zocial365.app.datas.ViewPager.CustomViewPager;
import kavita.zocial365.app.datas.http.GetJson;
import kavita.zocial365.app.datas.http.PostJSON;
import kavita.zocial365.app.datas.http.PutJson;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EditDetailFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EditDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditDetailFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    DetailActivity.SectionsPagerAdapter mSectionsPagerAdapter;
    private CustomViewPager mViewPager;
    private OnFragmentInteractionListener mListener;
    public TextView textViewGeographies;
    public TextView textViewProvinces;
    public TextView textViewAmphurs;
    public TextView textViewThumbons;
    public TextView textViewType;
    private TouristModel touristModel = new TouristModel();

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EditDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EditDetailFragment newInstance(String param1, String param2) {
        EditDetailFragment fragment = new EditDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public EditDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        mViewPager = ((DetailActivity) getActivity()).mViewPager;
        mSectionsPagerAdapter = ((DetailActivity) getActivity()).mSectionsPagerAdapter;
        touristModel = ((DetailActivity) getActivity()).touristModel;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_adding, container, false);
        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.layout_geo);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(6);
            }
        });
        LinearLayout linearLayout2 = (LinearLayout) view.findViewById(R.id.layout_provinces);
        linearLayout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(7);
            }
        });
        LinearLayout linearLayout3 = (LinearLayout) view.findViewById(R.id.layout_amphurs);
        linearLayout3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(8);
            }
        });
        LinearLayout linearLayout4 = (LinearLayout) view.findViewById(R.id.layout_thumbons);
        linearLayout4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(9);
            }
        });
        LinearLayout linearLayout5 = (LinearLayout) view.findViewById(R.id.layout_type);
        linearLayout5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DetailActivity) getActivity()).page_name = EditDetailFragment.class.getSimpleName();

                mViewPager.setCurrentItem(5);

            }
        });
        final EditText textViewLat = (EditText) view.findViewById(R.id.editTextLat);
        final EditText textViewLong = (EditText) view.findViewById(R.id.editTextLong);

        textViewGeographies = (TextView) view.findViewById(R.id.textView952);
        textViewProvinces = (TextView) view.findViewById(R.id.textView967);
        textViewAmphurs = (TextView) view.findViewById(R.id.textView976);
        textViewThumbons = (TextView) view.findViewById(R.id.textView985);
        textViewType = (TextView) view.findViewById(R.id.textView111);//ชื่อประเภท

        textViewType.setText(((DetailActivity) getActivity()).type_name);
        new ShowAddr(MyURL.HOST_NAME + "attraction_types/" + touristModel.getType_id() + ".json?auth_token="
                + MySession.getInstance().getUserModel().getAuth_token(), textViewType, "type_name_th").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        textViewGeographies.setText(((DetailActivity) getActivity()).geographies);
        new ShowAddr(MyURL.HOST_NAME + "location_geographies/" + touristModel.getRegion_id() + ".json?auth_token="
                + MySession.getInstance().getUserModel().getAuth_token(), textViewGeographies, "geo_name").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        textViewProvinces.setText(((DetailActivity) getActivity()).provinces);
        new ShowAddr(MyURL.HOST_NAME + "location_provinces/" + touristModel.getProvince_id() + ".json?auth_token="
                + MySession.getInstance().getUserModel().getAuth_token(), textViewProvinces, "province_name").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        textViewAmphurs.setText(((DetailActivity) getActivity()).amphurs);
        new ShowAddr(MyURL.HOST_NAME + "location_amphurs/" + touristModel.getAmphur_id() + ".json?auth_token="
                + MySession.getInstance().getUserModel().getAuth_token(), textViewAmphurs, "amphur_name").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        textViewThumbons.setText(((DetailActivity) getActivity()).thumbons);
        new ShowThumbon(MyURL.HOST_NAME + "location_thumbons.json?q[thumbon_id_eq]=" + touristModel.getThumbon_id() + "&limit=1&auth_token="
                + MySession.getInstance().getUserModel().getAuth_token(), textViewThumbons, "thumbon_name").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        textViewLat.setText(touristModel.getDegreex() + "");
        textViewLong.setText(touristModel.getDegreey() + "");
        try {
            final GPSTracker gpsTracker = new GPSTracker(getActivity());
//            textViewLat.setText(gpsTracker.getLatitude() + "");
//            textViewLong.setText(gpsTracker.getLongitude() + "");
            Button button = (Button) view.findViewById(R.id.buttonGetLocation);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i("Set location", "----------------------------");
                    textViewLat.setText(gpsTracker.getLatitude() + "");
                    textViewLong.setText(gpsTracker.getLongitude() + "");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        String time_rel = (touristModel.getTime_open().replace("น.", "")).replace(" ", "");
        StringTokenizer st = new StringTokenizer(time_rel, ".");

        String time_rel2 = (touristModel.getTime_close().replace("น.", "")).replace(" ", "");
        StringTokenizer st2 = new StringTokenizer(time_rel2, ".");

        final EditText editText1 = (EditText) view.findViewById(R.id.editText2);//ชื่อแหล่งท่องเที่ยว
        editText1.setText(touristModel.getName() + "");
        final EditText editText2 = (EditText) view.findViewById(R.id.editText3);//ชื่อแหล่งใหม่
        editText2.setText(touristModel.getName_form() + "");
        final EditText editText3 = (EditText) view.findViewById(R.id.editText5);//ชื่อหน่วยงาน
        editText3.setText(touristModel.getCare_agen() + "");
        final EditText editText4 = (EditText) view.findViewById(R.id.editText9);//คำอธิบาย
        editText4.setText(touristModel.getDescription() + "");
        final TimePicker timePickeropen = (TimePicker) view.findViewById(R.id.timePicker);//open editText.setText(touristModel.get);
        try {
            timePickeropen.setCurrentHour(Integer.valueOf(st.nextToken()));
            timePickeropen.setCurrentMinute(Integer.valueOf(st.nextToken()));
        } catch (Exception e) {
            e.printStackTrace();
        }


        final TimePicker timePickerclose = (TimePicker) view.findViewById(R.id.timePicker2);//close
        try {
            timePickerclose.setCurrentHour(Integer.valueOf(st2.nextToken()));
            timePickerclose.setCurrentMinute(Integer.valueOf(st2.nextToken()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        final EditText editText5 = (EditText) view.findViewById(R.id.editText7);//ค่าบริการเด็กไทย
        editText5.setText(touristModel.getFee_thai_child() + "");
        final EditText editText6 = (EditText) view.findViewById(R.id.editText6);//ผู้ใหญ่ไทย
        editText6.setText(touristModel.getFee_thai_adult() + "");
        final EditText editText7 = (EditText) view.findViewById(R.id.editText13);//เด็กฝรั่ง
        editText7.setText(touristModel.getFee_forient_child() + "");
        final EditText editText8 = (EditText) view.findViewById(R.id.editText11);//ผู้ใหญ่ฝรั่ง
        editText8.setText(touristModel.getFee_forient_adult() + "");
        final EditText editText9 = (EditText) view.findViewById(R.id.editText10);//ที่ตั้ง
        editText9.setText(touristModel.getAddr() + "");
        final EditText editText10 = (EditText) view.findViewById(R.id.editText16);//ถนน
        editText10.setText(touristModel.getRoad() + "");
        final EditText editText11 = (EditText) view.findViewById(R.id.editText24);//ไปรษณี
        editText11.setText(touristModel.getZipcode() + "");
        final EditText editText12 = (EditText) view.findViewById(R.id.editText23);//tel
        editText12.setText(touristModel.getTel() + "");
        final EditText editText13 = (EditText) view.findViewById(R.id.editText22);//fax
        editText13.setText(touristModel.getFax() + "");
        final EditText editText14 = (EditText) view.findViewById(R.id.editText21); //เวบ
        editText14.setText(touristModel.getWebsite() + "");
        final EditText editText15 = (EditText) view.findViewById(R.id.editText20);//เจ้าของกรรมสิท
        editText15.setText(touristModel.getOwn_land() + "");
        final EditText editText16 = (EditText) view.findViewById(R.id.editText26);//ข้อมูลเอกสารสิท
        editText16.setText(touristModel.getLot_discribe() + "");
        final EditText editText17 = (EditText) view.findViewById(R.id.editText25);//ข้อมูลหน่วยงานที่บริหาร
        editText17.setText(touristModel.getManage_agen() + "");
        final EditText editText18 = (EditText) view.findViewById(R.id.editText14);//ความนิยม
        editText18.setText(touristModel.getLoc_or_no1() + "");
        final EditText editText19 = (EditText) view.findViewById(R.id.editText19);//จำนวนไทยวัน
        editText19.setText(touristModel.getTh_tour_dialy() + "");
        final EditText editText20 = (EditText) view.findViewById(R.id.editText29);//เดือน ไทย
        editText20.setText(touristModel.getTh_tour_monthly() + "");
        final EditText editText21 = (EditText) view.findViewById(R.id.editText30);//ปี ไทย
        editText21.setText(touristModel.getTh_tour_yearly() + "");
        final EditText editText22 = (EditText) view.findViewById(R.id.editText32);//วัน ฝรั่ง
        editText22.setText(touristModel.getFor_tour_daily() + "");
        final EditText editText23 = (EditText) view.findViewById(R.id.editText31);//เดือน ฝรั่ง
        editText23.setText(touristModel.getFor_tour_monthly() + "");
        final EditText editText24 = (EditText) view.findViewById(R.id.editText28);//ปี ฝรั่ง
        editText24.setText(touristModel.getFor_tour_yearly() + "");
        Button button = (Button) view.findViewById(R.id.buttonSave);

        timePickeropen.setIs24HourView(true);
        timePickerclose.setIs24HourView(true);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[name]", editText1.getText().toString() + ""));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[name_form]", editText2.getText().toString() + ""));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[care_agen]", editText3.getText().toString() + ""));

                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[type_id]", ((DetailActivity) getActivity()).type + ""));

                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[description]", editText4.getText().toString() + ""));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[tour_day]", ""));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[time_open]", timePickeropen.getCurrentHour() + "." + timePickeropen.getCurrentMinute() + " น."));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[time_close]", timePickerclose.getCurrentHour() + "." + timePickerclose.getCurrentMinute() + " น."));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[fee_thai_child]", editText5.getText().toString() + ""));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[fee_thai_adult]", editText6.getText().toString() + ""));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[fee_forient_child]", editText7.getText().toString() + ""));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[fee_forient_adult]", editText8.getText().toString() + ""));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[degreex]", textViewLat.getText().toString() + ""));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[degreey]", textViewLong.getText().toString() + ""));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[addr]", editText9.getText().toString() + ""));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[road]", editText10.getText().toString() + ""));

                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[thumbon_id]", ((DetailActivity) getActivity()).thumbons_id + ""));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[amphur_id]", ((DetailActivity) getActivity()).amphurs_id + ""));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[province_id]", ((DetailActivity) getActivity()).provinces_id + ""));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[region_id]", ((DetailActivity) getActivity()).geographies_id + ""));

                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[zipcode]", editText11.getText().toString() + ""));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[tel]", editText12.getText().toString() + ""));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[fax]", editText13.getText().toString() + ""));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[website]", editText14.getText().toString() + ""));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[own_land]", editText15.getText().toString() + ""));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[lot_discribe]", editText16.getText().toString() + ""));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[manage_agen]", editText17.getText().toString() + ""));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[loc_or_no1]", editText18.getText().toString() + ""));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[th_tour_dialy]", editText19.getText().toString() + ""));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[th_tour_monthly]", editText20.getText().toString() + ""));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[th_tour_yearly]", editText21.getText().toString() + ""));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[for_tour_dialy]", editText22.getText().toString() + ""));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[for_tour_monthly]", editText23.getText().toString() + ""));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[for_tour_yearly]", editText24.getText().toString() + ""));

                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[user_id]", ""));

                new SendData(nameValuePairs).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        textViewType.setText(((DetailActivity) getActivity()).type_name);
        textViewGeographies.setText(((DetailActivity) getActivity()).geographies);
        textViewProvinces.setText(((DetailActivity) getActivity()).provinces);
        textViewAmphurs.setText(((DetailActivity) getActivity()).amphurs);
        textViewThumbons.setText(((DetailActivity) getActivity()).thumbons);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    class SendData extends AsyncTask<Void, Void, Boolean> {
        String url = MyURL.HOST_NAME + "tbl_tourist_ups/" + ((DetailActivity) getActivity()).id + ".json"
                + "?auth_token=" + MySession.getInstance().getUserModel().getAuth_token();


        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        private JSONObject jsonObject1;

        SendData(ArrayList<NameValuePair> nameValuePairs) {
            this.nameValuePairs = nameValuePairs;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            PutJson postJSON = new PutJson();
            String result = postJSON.putJson(url, nameValuePairs);
            Log.i("Result", result);
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(result);
                if (jsonObject.optString("status").equalsIgnoreCase("9200")) {
                    jsonObject1 = jsonObject.optJSONObject("objects");
                    return true;
                } else {
                    //     Toast.makeText(getActivity(), "Editing not success", Toast.LENGTH_SHORT);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {

            if (aBoolean) {
                Intent intent = new Intent(getActivity(), DetailActivity.class);
                intent.putExtra("id", jsonObject1.optString("id"));
                intent.putExtra("title", jsonObject1.optString("name"));
                intent.putExtra("tour_id", jsonObject1.optString("tour_id"));
                startActivity(intent);
                getActivity().finish();
            } else {
                Toast.makeText(getActivity(), "Created not success", Toast.LENGTH_SHORT);
            }
        }
    }

    class ShowAddr extends AsyncTask<Void, Void, Boolean> {
        String url = "", field_name = "";
        private JSONObject jsonObject1 = new JSONObject();

        TextView textView = new TextView(getActivity());

        ShowAddr(String url, TextView textView, String field_name) {
            this.url = url;
            this.textView = textView;
            this.field_name = field_name;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            GetJson getJson = new GetJson();
            String result = getJson.getJSON(url);
            // Log.i("Result", result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.optString("status").equalsIgnoreCase("9200")) {
                    jsonObject1 = jsonObject.optJSONObject("objects");
                    Log.i("Result", "" + jsonObject1);
                    return true;
                } else {
//                    Toast.makeText(getActivity(), "Created not success", Toast.LENGTH_SHORT);
                    return false;
                }
            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean) {
                try {
                    textView.setText(
                            jsonObject1.optString(field_name, "")
                                    + "");
                    if (field_name.equalsIgnoreCase("geo_name")) {
                        ((DetailActivity) getActivity()).geographies_id = jsonObject1.optString("geo_id");
                    } else if (field_name.equalsIgnoreCase("province_name")) {
                        ((DetailActivity) getActivity()).provinces_id = jsonObject1.optString("province_id");
                    } else if (field_name.equalsIgnoreCase("amphur_name")) {
                        ((DetailActivity) getActivity()).amphurs_id = jsonObject1.optString("amphur_id");
                    } else if (field_name.equalsIgnoreCase("type_name_th")) {
                        Log.i("JSONUB", jsonObject1.optString("type_id", ""));
                        ((DetailActivity) getActivity()).type = jsonObject1.optString("id");
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(getActivity(), "show not success", Toast.LENGTH_SHORT);
            }
        }
    }

    class ShowThumbon extends AsyncTask<Void, Void, Boolean> {
        String url = "", field_name = "";
        private JSONArray jsonObject1 = new JSONArray();

        TextView textView = new TextView(getActivity());

        ShowThumbon(String url, TextView textView, String field_name) {
            this.url = url;
            this.textView = textView;
            this.field_name = field_name;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            GetJson getJson = new GetJson();
            String result = getJson.getJSON(url);
            Log.i("Result", result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.optString("status").equalsIgnoreCase("9200")) {
                    jsonObject1 = jsonObject.optJSONArray("objects");
                    return true;
                } else {
                    //
                    return false;
                }
            } catch (JSONException e) {
                e.printStackTrace();

            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean) {
                try {
                    for (int i = 0; i < jsonObject1.length(); i++) {
                        JSONObject jsonObject = jsonObject1.optJSONObject(i);
                        textView.setText(jsonObject.optString(field_name, ""));

                        ((DetailActivity) getActivity()).thumbons_id = jsonObject.optString("thumbons_id");

                    }

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(getActivity(), "Created not success", Toast.LENGTH_SHORT);
            }
        }
    }
}
