package kavita.zocial365.app.datas.ImageLoader;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;

import java.io.File;

import kavita.zocial365.app.datas.R;

/**
 * Created by kugking on 12/8/14 AD.
 */
public class MyImageLoderSetting {

    public DisplayImageOptions options = null;
    public ImageLoaderConfiguration configuration = null;
    private static MyImageLoderSetting instance;

    private MyImageLoderSetting() {

    }

    ;

    public static MyImageLoderSetting getInatance(Context context) {
        if (instance == null) {
            instance = new MyImageLoderSetting();

        }

        return instance;

    }

    public void setting(Context context) {
        String folder = "/Dasta/cache/";
        String path = Environment.getExternalStorageDirectory() + folder;
        File cacheDir = new File(path);
        // File cacheDir = StorageUtils.getCacheDirectory(context);
//		options = new DisplayImageOptions.Builder()
//				.showImageOnLoading(R.drawable.border)
//				.showImageForEmptyUri(R.drawable.ic_action_error).cacheOnDisc()
//				.cacheOnDisc(true).cacheInMemory(true).cacheInMemory()
//				.bitmapConfig(Bitmap.Config.ARGB_8888)
//				.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
//				.displayer(new SimpleBitmapDisplayer())
//
//				.build();
//
//		configuration = new ImageLoaderConfiguration.Builder(context)
//				.threadPoolSize(10)
//				.threadPriority(Thread.NORM_PRIORITY - 1)
//				.tasksProcessingOrder(QueueProcessingType.FIFO)
//				.defaultDisplayImageOptions(options)
//
//				.memoryCacheSize(4 * 1024 * 1024)
//				.memoryCacheSizePercentage(50)
//				.memoryCache(new WeakMemoryCache())
//				// .memoryCache(new LruMemoryCache(10 * 1024 * 1024))
//				// .memoryCacheSize(4 * 1024 * 1024)
//
//				.denyCacheImageMultipleSizesInMemory()
//				.discCache(new UnlimitedDiscCache(cacheDir))
//				.discCacheSize(4 * 1024 * 1024).writeDebugLogs()
//
//				.build();

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_action_picture)
                .showImageOnFail(R.drawable.ic_action_picture)
                .showImageForEmptyUri(R.drawable.ic_action_picture)
                .cacheInMemory(false)
                .bitmapConfig(Bitmap.Config.RGB_565).cacheOnDisc(true)
                .displayer(new SimpleBitmapDisplayer())
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565).build();

        configuration = new ImageLoaderConfiguration.Builder(
                context.getApplicationContext()).threadPoolSize(5)
                .threadPriority(Thread.NORM_PRIORITY - 1)
                .tasksProcessingOrder(QueueProcessingType.FIFO)
                .defaultDisplayImageOptions(options)
                        //.memoryCache(new WeakMemoryCache())
                .discCache(new UnlimitedDiscCache(cacheDir))

				/*.memoryCacheSizePercentage(13)
                .denyCacheImageMultipleSizesInMemory()
				.discCache(new UnlimitedDiscCache(StoreVariable.file))
				.discCacheSize(4 * 1024 * 1024).writeDebugLogs()*/.build();
        ImageLoader.getInstance().init(configuration);
    }
}
