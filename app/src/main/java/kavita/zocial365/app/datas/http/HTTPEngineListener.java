package kavita.zocial365.app.datas.http;

import kavita.zocial365.app.datas.dao.BaseDao;


/**
 * Created by nuuneoi on 10/14/2014.
 */
public interface HTTPEngineListener<T extends BaseDao> {

    public void onResponse(T data, String rawData);

    public void onFailure(T data, String rawData);

}
