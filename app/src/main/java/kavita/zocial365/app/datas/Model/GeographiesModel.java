package kavita.zocial365.app.datas.Model;

/**
 * Created by kugking on 12/11/14 AD.
 */
public class GeographiesModel {
    String id = "", geo_id = "", geo_name = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGeo_id() {
        return geo_id;
    }

    public void setGeo_id(String geo_id) {
        this.geo_id = geo_id;
    }

    public String getGeo_name() {
        return geo_name;
    }

    public void setGeo_name(String geo_name) {
        this.geo_name = geo_name;
    }
}
