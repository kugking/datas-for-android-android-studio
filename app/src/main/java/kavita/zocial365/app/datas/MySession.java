package kavita.zocial365.app.datas;

import android.util.Log;

import kavita.zocial365.app.datas.Model.UserModel;

/**
 * Created by kugking on 1/12/15 AD.
 */
public class MySession {
    private UserModel userModel;
    static MySession instance = null;

    private MySession() {

    }

    public static MySession getInstance() {
        if (instance == null) {
            instance = new MySession();
        }
        return instance;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        //Log.i("MySession","Auth id :"+ userModel.getAuth_token()+" , Name :"+userModel.getName());
        this.userModel = userModel;
    }

}
