package kavita.zocial365.app.datas.Model;

/**
 * Created by kugking on 12/12/14 AD.
 */
public class ThumbonsModel {
    String id = "id", amphur_id = "amphur_id", thumbon_id = "thumbon_id", thumbon_name = "thumbon_name";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAmphur_id() {
        return amphur_id;
    }

    public void setAmphur_id(String amphur_id) {
        this.amphur_id = amphur_id;
    }

    public String getThumbon_id() {
        return thumbon_id;
    }

    public void setThumbon_id(String thumbon_id) {
        this.thumbon_id = thumbon_id;
    }

    public String getThumbon_name() {
        return thumbon_name;
    }

    public void setThumbon_name(String thumbon_name) {
        this.thumbon_name = thumbon_name;
    }
}
