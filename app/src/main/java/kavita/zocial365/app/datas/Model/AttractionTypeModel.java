package kavita.zocial365.app.datas.Model;

/**
 * Created by kugking on 12/3/14 AD.
 */
public class AttractionTypeModel {
    String id,type_name_th,type_type_nm_t;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType_name_th() {
        return type_name_th;
    }

    public void setType_name_th(String type_name_th) {
        this.type_name_th = type_name_th;
    }

    public String getType_type_nm_t() {
        return type_type_nm_t;
    }

    public void setType_type_nm_t(String type_type_nm_t) {
        this.type_type_nm_t = type_type_nm_t;
    }
}
