package kavita.zocial365.app.datas.Detail;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;

import kavita.zocial365.app.datas.DetailActivity;
import kavita.zocial365.app.datas.Model.ImagesModel;
import kavita.zocial365.app.datas.Model.TouristModel;
import kavita.zocial365.app.datas.MySession;
import kavita.zocial365.app.datas.MyURL;
import kavita.zocial365.app.datas.R;
import kavita.zocial365.app.datas.http.GetJson;
import kavita.zocial365.app.datas.json.ChangeJson;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DetailFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ViewPager mViewPager;
    DetailActivity.SectionsPagerAdapter mSectionsPagerAdapter;
    private OnFragmentInteractionListener mListener;
    String id = "", tour_id = "";
    private TouristModel touristModel = new TouristModel();
    private TextView textViewTitle;
    private TextView textViewPoint;
    private TextView textViewDate;
    private TextView textViewDescription;
    private TextView textViewTitle2;
    private ImageView imageView;
    private String limit = "15";
    private ImageView imageView2;
    Bitmap bitmapImageDefault;
    private TextView textViewname;
    private TextView textViewname_form;
    private TextView textViewcare_agen;
    private TextView textViewdate_create;
    private TextView textViewtype_id;
    private TextView textViewDescription1;
    private TextView textViewDateOpen;
    private TextView textViewfee_thai_child;
    private TextView textViewfee_thai_adult;
    private TextView textViewfee_forient_child;
    private TextView textViewfee_forient_adult;
    private TextView textViewdegreex;
    private TextView textViewdegreey;
    private TextView textViewaddr;
    private TextView textViewroad;
    private TextView textViewthumbon_id;
    private TextView textViewamphur_id;
    private TextView textViewprovince_id;
    private TextView textViewzipcode;
    private TextView textViewtel;
    private TextView textViewfax;
    private TextView textViewwebsite;
    private TextView textViewown_land;
    private TextView textViewlot_discribe;
    private TextView textViewmanage_agen;
    private TextView textViewth_tour_dialy;
    private TextView textViewth_tour_monthly;
    private TextView textViewth_tour_yearly;
    private TextView textViewfor_tour_daily;
    private TextView textViewfor_tour_monthly;
    private TextView textViewfor_tour_yearly;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DetailFragment newInstance(String param1, String param2) {
        DetailFragment fragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public DetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        mViewPager = ((DetailActivity) getActivity()).mViewPager;
        mSectionsPagerAdapter = ((DetailActivity) getActivity()).mSectionsPagerAdapter;
        id = mParam1;
        tour_id = mParam2;
        textViewTitle2 = ((DetailActivity) getActivity()).textViewTitle;
        bitmapImageDefault = BitmapFactory.decodeResource(getResources(), R.drawable.ic_action_picture);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        textViewTitle = (TextView) view.findViewById(R.id.textViewTittle);
        imageView = (ImageView) view.findViewById(R.id.imageView2);
        imageView2 = (ImageView) view.findViewById(R.id.imageView1);
        textViewPoint = (TextView) view.findViewById(R.id.textView4);
        textViewDate = (TextView) view.findViewById(R.id.textViewDate);
        textViewDescription = (TextView) view.findViewById(R.id.textViewDescription);
        textViewname = (TextView) view.findViewById(R.id.textView_name);
        textViewname_form = (TextView) view.findViewById(R.id.textView77);
        textViewcare_agen = (TextView) view.findViewById(R.id.textView78);
        textViewdate_create = (TextView) view.findViewById(R.id.textView);
        textViewtype_id = (TextView) view.findViewById(R.id.textView79);
        textViewDescription1 = (TextView) view.findViewById(R.id.textView80);
        textViewDateOpen = (TextView) view.findViewById(R.id.textView81);
        textViewfee_thai_child = (TextView) view.findViewById(R.id.textView82);
        textViewfee_thai_adult = (TextView) view.findViewById(R.id.textView83);
        textViewfee_forient_child = (TextView) view.findViewById(R.id.textView84);
        textViewfee_forient_adult = (TextView) view.findViewById(R.id.textView85);
        textViewdegreex = (TextView) view.findViewById(R.id.textView86);
        textViewdegreey = (TextView) view.findViewById(R.id.textView87);
        textViewaddr = (TextView) view.findViewById(R.id.textView88);
        textViewroad = (TextView) view.findViewById(R.id.textView89);
        textViewthumbon_id = (TextView) view.findViewById(R.id.textView90);
        textViewamphur_id = (TextView) view.findViewById(R.id.textView91);
        textViewprovince_id = (TextView) view.findViewById(R.id.textView92);
        textViewzipcode = (TextView) view.findViewById(R.id.textView93);
        textViewtel = (TextView) view.findViewById(R.id.textView110);
        textViewfax = (TextView) view.findViewById(R.id.textView112);
        textViewwebsite = (TextView) view.findViewById(R.id.textView113);
        textViewown_land = (TextView) view.findViewById(R.id.textView114);
        textViewlot_discribe = (TextView) view.findViewById(R.id.textView115);
        textViewmanage_agen = (TextView) view.findViewById(R.id.textView116);
        textViewth_tour_dialy = (TextView) view.findViewById(R.id.textView117);
        textViewth_tour_monthly = (TextView) view.findViewById(R.id.textView118);
        textViewth_tour_yearly = (TextView) view.findViewById(R.id.textView119);
        textViewfor_tour_daily = (TextView) view.findViewById(R.id.textView76);
        textViewfor_tour_monthly = (TextView) view.findViewById(R.id.textView75);
        textViewfor_tour_yearly = (TextView) view.findViewById(R.id.textView74);

        LinearLayout linearLayoutShowAllImage = (LinearLayout) view.findViewById(R.id.linearlayoutShowAllImages);
        linearLayoutShowAllImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mViewPager.setCurrentItem(3);
            }
        });
        LinearLayout linearLayoutEvaluate = (LinearLayout) view.findViewById(R.id.linearlayoutEvaluate);
        LinearLayout linearLayoutAssessment = (LinearLayout) view.findViewById(R.id.linearlayoutAssessment);
        linearLayoutEvaluate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(0);
            }
        });
        linearLayoutAssessment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                AssessmentFragment assessmentFragment = AssessmentFragment.newInstance("", "");
//                mSectionsPagerAdapter.add(assessmentFragment);
//                Intent intent = new Intent(getActivity(), AssessmentActivity.class);
//                startActivity(intent);
                mViewPager.setCurrentItem(2);
            }
        });
        new DownloadDetail().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    class DownloadDetail extends AsyncTask<Void, Void, Boolean> {

        private String SUMSCORE = "sum_score";

        DownloadDetail() {

        }

        @Override
        protected Boolean doInBackground(Void... params) {
            String url = MyURL.HOST_NAME + "tbl_tourist_ups/" + tour_id + ".json?auth_token=" + MySession.getInstance().getUserModel().getAuth_token();
            //     Log.i("URL Detail", "Detail url:" + url);
            GetJson getJson = new GetJson();
            String result = getJson.getJSON(url);
            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.optString("status").equalsIgnoreCase("9200")) {
                    JSONObject jsonObject1 = jsonObject.optJSONObject("objects");
                    ChangeJson changeJson = new ChangeJson();
                    touristModel = changeJson.addTouristModel(jsonObject1);
                    ((DetailActivity) getActivity()).touristModel = touristModel;
                    return true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (aBoolean) {

                textViewTitle.setText(touristModel.getName());
                if (!(touristModel.getEdit_date() + "").equalsIgnoreCase("null")) {
                    Log.i("Set score", "1 ," + touristModel.getEdit_date());
                    textViewDate.setText(touristModel.getEdit_date());
                } else {
                    Log.i("Set score", "2 ," + touristModel.getDate_create());
                    if (!(touristModel.getDate_create() + "").equalsIgnoreCase("null")) {
                        textViewDate.setText(touristModel.getDate_create());
                    } else {
                        textViewDate.setText(" - ");
                    }

                }
                // textViewDate.setText(touristModel.getEdit_date());
                textViewDescription.setText(touristModel.getDescription() + "");
                textViewDescription1.setText(touristModel.getDescription() + "");
                textViewname.setText(touristModel.getName() + "");
                textViewname_form.setText(touristModel.getName_form() + "");
                textViewcare_agen.setText(touristModel.getCare_agen() + "");
                textViewtype_id.setText(touristModel.getType_id() + "");
                textViewDateOpen.setText(touristModel.getTime_open() + " - " + touristModel.getTime_close());
                textViewfee_thai_adult.setText(touristModel.getFee_thai_adult() + "");
                textViewfee_thai_child.setText(touristModel.getFee_thai_child() + "");
                textViewfee_forient_child.setText(touristModel.getFee_forient_child() + "");
                textViewfee_forient_adult.setText(touristModel.getFee_forient_adult() + "");
                textViewdegreex.setText(touristModel.getDegreex() + "");
                textViewdegreey.setText(touristModel.getDegreey() + "");
                textViewaddr.setText(touristModel.getAddr() + "");
                textViewroad.setText(touristModel.getRoad() + "");
                textViewthumbon_id.setText(touristModel.getThumbon_id() + "");
                textViewamphur_id.setText(touristModel.getAmphur_id() + "");
                textViewprovince_id.setText(touristModel.getProvince_id() + "");
                textViewzipcode.setText(touristModel.getZipcode() + "");
                textViewtel.setText(touristModel.getTel() + "");
                textViewfax.setText(touristModel.getFax() + "");
                textViewwebsite.setText(touristModel.getWebsite() + "");
                textViewown_land.setText(touristModel.getOwn_land() + "");
                textViewlot_discribe.setText(touristModel.getLot_discribe() + "");
                textViewmanage_agen.setText(touristModel.getManage_agen() + "");
                textViewth_tour_dialy.setText(touristModel.getTh_tour_dialy() + "");
                textViewth_tour_monthly.setText(touristModel.getTh_tour_monthly() + "");
                textViewth_tour_yearly.setText(touristModel.getTh_tour_yearly() + "");
                textViewfor_tour_daily.setText(touristModel.getFor_tour_daily() + "");
                textViewfor_tour_monthly.setText(touristModel.getFor_tour_monthly() + "");
                textViewfor_tour_yearly.setText(touristModel.getFor_tour_yearly() + "");
                try {
                    // Log.i("SUM", touristModel.getTbl_tourist_eva_summary().toString());

                    textViewPoint.setText(touristModel.getTbl_tourist_eva_summary().optJSONObject(0).optString(SUMSCORE, ""));
                } catch (Exception e) {

                    e.printStackTrace();
                }

                textViewTitle2.setText(touristModel.getName());
                try {
                    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
                    imageLoader.displayImage(touristModel.getImagesModels().get(0).getImage_url(), imageView);
                    imageLoader.displayImage(touristModel.getImagesModels().get(0).getImage_thumb_url(), imageView2);

                } catch (Exception e) {
                    e.printStackTrace();
                    imageView.setImageBitmap(bitmapImageDefault);
                    imageView2.setImageBitmap(bitmapImageDefault);
                }


                new ShowAddr(MyURL.HOST_NAME + "attraction_types/" + touristModel.getType_id() + ".json?auth_token="
                        + MySession.getInstance().getUserModel().getAuth_token(), textViewtype_id, "type_name_th").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                // new ShowAddr(MyURL.HOST_NAME + "location_geographies/" + touristModel.getRegion_id()
                // + ".json", textViewge, "geo_name").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                new ShowAddr(MyURL.HOST_NAME + "location_provinces/" + touristModel.getProvince_id() + ".json?auth_token="
                        + MySession.getInstance().getUserModel().getAuth_token(), textViewprovince_id, "province_name").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                new ShowAddr(MyURL.HOST_NAME + "location_amphurs/" + touristModel.getAmphur_id() + ".json?auth_token="
                        + MySession.getInstance().getUserModel().getAuth_token(), textViewamphur_id, "amphur_name").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                new ShowThumbon(MyURL.HOST_NAME + "location_thumbons.json?q[thumbon_id_eq]=" + touristModel.getThumbon_id() + "&limit=1&auth_token="
                        + MySession.getInstance().getUserModel().getAuth_token(), textViewthumbon_id, "thumbon_name").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


            }

            super.onPostExecute(aBoolean);
        }
    }

    class ImageLoader extends AsyncTask<Void, Void, Boolean> {
        Bitmap bitmap;
        String url;
        ImageView imageView;
        com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();

        ImageLoader(ImageView imageView, String url) {
            this.imageView = imageView;
            this.url = url;

        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                bitmap = imageLoader.loadImageSync(url);
                return true;
            } catch (Exception ex) {
                ex.printStackTrace();

            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean) {
                imageView.setImageBitmap(bitmap);

            }
        }
    }

    class DownloadImage extends AsyncTask<Void, Void, Boolean> {
        String img_id = "";
        int position = 0;
        ImageView imageView;

        DownloadImage(String img_id, int posotion, ImageView imageView) {
            this.img_id = img_id;
            this.position = posotion;
            this.imageView = imageView;

        }

        @Override
        protected Boolean doInBackground(Void... params) {
            GetJson getJson = new GetJson();
            String url = MyURL.HOST_NAME + "tbl_tourist_images.json?q[tour_id_eq]=" + tour_id +
                    "&limit=" + limit + "&auth_token=" + MySession.getInstance().getUserModel().getAuth_token();
            //  Log.i("Image URL", "url :" + url);
            String result = getJson.getJSON(url);
            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.optString("status", "").equalsIgnoreCase("9200")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("objects");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.optJSONObject(i);
//                        touristModels.get(position).setId_img(jsonObject1.optString("img_id"));
//                        touristModels.get(position).setPath_img(jsonObject1.optString("path"));
//                        touristModels.get(position).setId_img(jsonObject1.optString("id"));
                        ImagesModel imagesModel = new ImagesModel();
                        imagesModel.setId(jsonObject1.optString("id"));
                        imagesModel.setImg_id(jsonObject1.optString("img_id"));
                        imagesModel.setPath(jsonObject1.optString("path"));
                        imagesModel.setTour_id(jsonObject1.optString("tour_id"));
                        touristModel.addImageModel(imagesModel);
                        // imagesModels.add(imagesModel);
                    }

                    return true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return true;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);

        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean) {
                try {
                    new ImageLoader(imageView, "http://tis.dasta.or.th/dasta_survey/tourist_profile_img/" + touristModel.getImagesModels().get(0).getPath()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }

        }
    }


    class ShowAddr extends AsyncTask<Void, Void, Boolean> {
        String url = "", field_name = "";
        private JSONObject jsonObject1 = new JSONObject();

        TextView textView = new TextView(getActivity());

        ShowAddr(String url, TextView textView, String field_name) {
            this.url = url;
            this.textView = textView;
            this.field_name = field_name;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            GetJson getJson = new GetJson();
            String result = getJson.getJSON(url);
             Log.i("Result", field_name+" : "+result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.optString("status").equalsIgnoreCase("9200")) {
                    jsonObject1 = jsonObject.optJSONObject("objects");
                    Log.i("Result", "" + jsonObject1);
                    return true;
                } else {
//                    Toast.makeText(getActivity(), "Created not success", Toast.LENGTH_SHORT);
                    return false;
                }
            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean) {
                try {
                    textView.setText(
                            jsonObject1.optString(field_name, "")
                                    + "");
                    if (field_name.equalsIgnoreCase("geo_name")) {
                        ((DetailActivity) getActivity()).geographies_id = jsonObject1.optString("geo_id");
                    } else if (field_name.equalsIgnoreCase("province_name")) {
                        ((DetailActivity) getActivity()).provinces_id = jsonObject1.optString("province_id");
                    } else if (field_name.equalsIgnoreCase("amphur_name")) {
                        ((DetailActivity) getActivity()).amphurs_id = jsonObject1.optString("amphur_id");
                    } else if (field_name.equalsIgnoreCase("type_name_th")) {
                        Log.i("JSONUB", jsonObject1.optString("type_id", ""));
                        ((DetailActivity) getActivity()).type = jsonObject1.optString("id");
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(getActivity(), "show not success", Toast.LENGTH_SHORT);
            }
        }
    }

    class ShowThumbon extends AsyncTask<Void, Void, Boolean> {
        String url = "", field_name = "";
        private JSONArray jsonObject1 = new JSONArray();

        TextView textView = new TextView(getActivity());

        ShowThumbon(String url, TextView textView, String field_name) {
            this.url = url;
            this.textView = textView;
            this.field_name = field_name;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            GetJson getJson = new GetJson();
            String result = getJson.getJSON(url);
            Log.i("Result", result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.optString("status").equalsIgnoreCase("9200")) {
                    jsonObject1 = jsonObject.optJSONArray("objects");
                    return true;
                } else {
                    //
                    return false;
                }
            } catch (JSONException e) {
                e.printStackTrace();

            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean) {
                try {
                    for (int i = 0; i < jsonObject1.length(); i++) {
                        JSONObject jsonObject = jsonObject1.optJSONObject(i);
                        textView.setText(jsonObject.optString(field_name, ""));

                        ((DetailActivity) getActivity()).thumbons_id = jsonObject.optString("thumbons_id");

                    }

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(getActivity(), "Created not success", Toast.LENGTH_SHORT);
            }
        }
    }
}
