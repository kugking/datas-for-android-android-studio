package kavita.zocial365.app.datas.Home;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Handler;

import kavita.zocial365.app.datas.DetailActivity;
import kavita.zocial365.app.datas.GPS.GPSTracker;
import kavita.zocial365.app.datas.HomeActivity;
import kavita.zocial365.app.datas.MySession;
import kavita.zocial365.app.datas.MyURL;
import kavita.zocial365.app.datas.ViewPager.CustomViewPager;
import kavita.zocial365.app.datas.R;
import kavita.zocial365.app.datas.http.PostJSON;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AddingFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AddingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddingFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    HomeActivity.SectionsPagerAdapter mSectionsPagerAdapter;
    private CustomViewPager mViewPager;
    public TextView textViewGeographies;
    public TextView textViewProvinces;
    public TextView textViewAmphurs;
    public TextView textViewThumbons;
    public TextView textViewType;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddingFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddingFragment newInstance(String param1, String param2) {
        AddingFragment fragment = new AddingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public AddingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
//        TextView textViewTitle = (TextView) getActivity().findViewById(R.id.textViewTittle);
//        textViewTitle.setText("สำรวจแหล่งท่องเที่ยว");
        mViewPager = ((HomeActivity) getActivity()).mViewPager;
        Log.i("AddingFragment","onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i("AddingFragment","onCreateView");
        View view = inflater.inflate(R.layout.fragment_adding, container, false);
        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.layout_geo);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(4);
            }
        });
        LinearLayout linearLayout2 = (LinearLayout) view.findViewById(R.id.layout_provinces);
        linearLayout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(5);
            }
        });
        LinearLayout linearLayout3 = (LinearLayout) view.findViewById(R.id.layout_amphurs);
        linearLayout3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(6);
            }
        });
        LinearLayout linearLayout4 = (LinearLayout) view.findViewById(R.id.layout_thumbons);
        linearLayout4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(7);
            }
        });
        LinearLayout linearLayout5 = (LinearLayout) view.findViewById(R.id.layout_type);
        linearLayout5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity) getActivity()).page_name = AddingFragment.class.getSimpleName();

                mViewPager.setCurrentItem(3);

            }
        });
        final EditText textViewLat = (EditText) view.findViewById(R.id.editTextLat);
        final EditText textViewLong = (EditText) view.findViewById(R.id.editTextLong);

        textViewGeographies = (TextView) view.findViewById(R.id.textView952);
        textViewProvinces = (TextView) view.findViewById(R.id.textView967);
        textViewAmphurs = (TextView) view.findViewById(R.id.textView976);
        textViewThumbons = (TextView) view.findViewById(R.id.textView985);
        textViewType = (TextView) view.findViewById(R.id.textView111);//ชื่อประเภท

        textViewType.setText(((HomeActivity) getActivity()).type_name);
        textViewGeographies.setText(((HomeActivity) getActivity()).geographies);
        textViewProvinces.setText(((HomeActivity) getActivity()).provinces);
        textViewAmphurs.setText(((HomeActivity) getActivity()).amphurs);
        textViewThumbons.setText(((HomeActivity) getActivity()).thumbons);
        try {
            final GPSTracker gpsTracker = new GPSTracker(getActivity());
            textViewLat.setText(gpsTracker.getLatitude() + "");
            textViewLong.setText(gpsTracker.getLongitude() + "");
            Button button = (Button) view.findViewById(R.id.buttonGetLocation);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    textViewLat.setText(gpsTracker.getLatitude() + "");
                    textViewLong.setText(gpsTracker.getLongitude() + "");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        final EditText editText1 = (EditText) view.findViewById(R.id.editText2);//ชื่อแหล่งท่องเที่ยว
        final EditText editText2 = (EditText) view.findViewById(R.id.editText3);//ชื่อแหล่งใหม่
        final EditText editText3 = (EditText) view.findViewById(R.id.editText5);//ชื่อหน่วยงาน
        final EditText editText4 = (EditText) view.findViewById(R.id.editText9);//คำอธิบาย
        final TimePicker timePickeropen = (TimePicker) view.findViewById(R.id.timePicker);//open
        final TimePicker timePickerclose = (TimePicker) view.findViewById(R.id.timePicker2);//close
        final EditText editText5 = (EditText) view.findViewById(R.id.editText7);//ค่าบริการเด็กไทย
        final EditText editText6 = (EditText) view.findViewById(R.id.editText6);//ผู้ใหญ่ไทย
        final EditText editText7 = (EditText) view.findViewById(R.id.editText13);//เด็กฝรั่ง
        final EditText editText8 = (EditText) view.findViewById(R.id.editText11);//ผู้ใหญ่ฝรั่ง
        final EditText editText9 = (EditText) view.findViewById(R.id.editText10);//ที่ตั้ง
        final EditText editText10 = (EditText) view.findViewById(R.id.editText16);//ถนน
        final EditText editText11 = (EditText) view.findViewById(R.id.editText24);//ไปรษณี
        final EditText editText12 = (EditText) view.findViewById(R.id.editText23);//tel
        final EditText editText13 = (EditText) view.findViewById(R.id.editText22);//fax
        final EditText editText14 = (EditText) view.findViewById(R.id.editText21); //เวบ
        final EditText editText15 = (EditText) view.findViewById(R.id.editText20);//เจ้าของกรรมสิท
        final EditText editText16 = (EditText) view.findViewById(R.id.editText26);//ข้อมูลเอกสารสิท
        final EditText editText17 = (EditText) view.findViewById(R.id.editText25);//ข้อมูลหน่วยงานที่บริหาร
        final EditText editText18 = (EditText) view.findViewById(R.id.editText14);//ความนิยม
        final EditText editText19 = (EditText) view.findViewById(R.id.editText19);//จำนวนไทยวัน
        final EditText editText20 = (EditText) view.findViewById(R.id.editText29);//เดือน ไทย
        final EditText editText21 = (EditText) view.findViewById(R.id.editText30);//ปี ไทย
        final EditText editText22 = (EditText) view.findViewById(R.id.editText32);//วัน ฝรั่ง
        final EditText editText23 = (EditText) view.findViewById(R.id.editText31);//เดือน ฝรั่ง
        final EditText editText24 = (EditText) view.findViewById(R.id.editText28);//ปี ฝรั่ง
        Button button = (Button) view.findViewById(R.id.buttonSave);

        timePickeropen.setIs24HourView(true);
        timePickerclose.setIs24HourView(true);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[name]", editText1.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[name_form]", editText2.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[care_agen]", editText3.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[type_id]", ((HomeActivity) getActivity()).type));
//                nameValuePairs.add(new BasicNameValuePair("type_type_nm_t", ""));
//                nameValuePairs.add(new BasicNameValuePair("sec_type_nm_t", ""));
//                nameValuePairs.add(new BasicNameValuePair("type_sub_nm_t", ""));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[description]", editText4.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[tour_day]", ""));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[time_open]", timePickeropen.getCurrentHour() + "." + timePickeropen.getCurrentMinute()+" น."));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[time_close]", timePickerclose.getCurrentHour() + "." + timePickerclose.getCurrentMinute()+" น."));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[fee_thai_child]", editText5.getText().toString() ));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[fee_thai_adult]", editText6.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[fee_forient_child]", editText7.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[fee_forient_adult]", editText8.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[degreex]", textViewLat.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[degreey]", textViewLong.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[addr]", editText9.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[road]", editText10.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[thumbon_id]", ((HomeActivity) getActivity()).thumbons_id));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[amphur_id]", ((HomeActivity) getActivity()).amphurs_id));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[province_id]", ((HomeActivity) getActivity()).provinces_id));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[region_id]", ((HomeActivity) getActivity()).geographies_id));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[zipcode]", editText11.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[tel]", editText12.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[fax]", editText13.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[website]", editText14.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[own_land]", editText15.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[lot_discribe]", editText16.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[manage_agen]", editText17.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[loc_or_no1]", editText18.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[th_tour_dialy]", editText19.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[th_tour_monthly]", editText20.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[th_tour_yearly]", editText21.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[for_tour_dialy]", editText22.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[for_tour_monthly]", editText23.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[for_tour_yearly]", editText24.getText().toString()));
//                nameValuePairs.add(new BasicNameValuePair("status", editText.getText().toString()));
//                nameValuePairs.add(new BasicNameValuePair("status_approve", editText.getText().toString()));
//                nameValuePairs.add(new BasicNameValuePair("dasta_id", editText.getText().toString()));
//                nameValuePairs.add(new BasicNameValuePair("status_low_carbon", editText.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("tbl_tourist_up[user_id]", ""));
//                nameValuePairs.add(new BasicNameValuePair("degen_status", editText.getText().toString()));
//                nameValuePairs.add(new BasicNameValuePair("", editText.getText().toString()));
                new SendData(nameValuePairs).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });

        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
//        android.os.Handler handler = getActivity().getWindow().getDecorView().getHandler();
//        handler.post(new Runnable() {
//            @Override public void run() {
//                // initialize the loader here!
//
//            }
//        });

   //     Log.i("Test 11", ((HomeActivity) getActivity()).type_name);
        textViewType.setText(((HomeActivity) getActivity()).type_name);
        textViewGeographies.setText(((HomeActivity) getActivity()).geographies);
        textViewProvinces.setText(((HomeActivity) getActivity()).provinces);
        textViewAmphurs.setText(((HomeActivity) getActivity()).amphurs);
        textViewThumbons.setText(((HomeActivity) getActivity()).thumbons);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    class SendData extends AsyncTask<Void, Void, Boolean> {
        String url = MyURL.ADDING_URL+"?auth_token="+ MySession.getInstance().getUserModel().getAuth_token();
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        private JSONObject jsonObject1= new JSONObject();;

        SendData(ArrayList<NameValuePair> nameValuePairs) {
            this.nameValuePairs = nameValuePairs;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            PostJSON postJSON = new PostJSON();
            String result= postJSON.postJSON(url, nameValuePairs);
            Log.i("Result",url+" , "+result);
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(result);
                if (jsonObject.optString("status").equalsIgnoreCase("9200")) {
                    jsonObject1 = jsonObject.optJSONObject("objects");
                    return true;
                }else {
                    Toast.makeText(getActivity(),"Created not success",Toast.LENGTH_SHORT);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return false;
        }
        @Override
        protected void onPostExecute(Boolean aBoolean) {

            if (aBoolean) {
                Intent intent = new Intent(getActivity(), DetailActivity.class);
                intent.putExtra("id",jsonObject1.optString("id") );
                intent.putExtra("title", jsonObject1.optString("name"));
                intent.putExtra("tour_id", jsonObject1.optString("tour_id"));
                startActivity(intent);
                mViewPager.setCurrentItem(1);
                //getActivity().finish();
            }
        }
    }
}
