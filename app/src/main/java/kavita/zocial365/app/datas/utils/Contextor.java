package kavita.zocial365.app.datas.utils;

import android.content.Context;

/**
 * Created by kugking on 12/18/14 AD.
 */
public class Contextor {
    private static Contextor instance;

    public static Contextor getInstance() {
        if (instance == null) {
            instance = new Contextor();
        }
        return instance;

    }

    private Context mContext;

    public Contextor() {

    }

    public void init(Context contextor) {
        mContext = contextor;
    }

    public Context getContext() {
        return mContext;
    }
}
