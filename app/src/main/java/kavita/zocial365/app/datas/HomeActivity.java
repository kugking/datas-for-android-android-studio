package kavita.zocial365.app.datas;

import java.util.ArrayList;
import java.util.Locale;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Build;
import android.support.v13.app.FragmentPagerAdapter;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import kavita.zocial365.app.datas.Home.AddingFragment;
import kavita.zocial365.app.datas.Home.AmphursFragment;
import kavita.zocial365.app.datas.Home.AttractionTypesFragment;
import kavita.zocial365.app.datas.Home.GeographiesFragment;
import kavita.zocial365.app.datas.Home.HelpFragment;
import kavita.zocial365.app.datas.Home.MapFragment;
import kavita.zocial365.app.datas.Home.ProvincesFragment;
import kavita.zocial365.app.datas.Home.SearchFragment;
import kavita.zocial365.app.datas.Home.ThumbonsFragment;
import kavita.zocial365.app.datas.ImageLoader.MyImageLoderSetting;
import kavita.zocial365.app.datas.ViewPager.CustomViewPager;


public class HomeActivity extends FragmentActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v13.app.FragmentStatePagerAdapter}.
     */
    public SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    public CustomViewPager mViewPager;
    public String type = "";
    public String type_name = "เลือกประเภท";
    public String geographies = "เลือกภาค", provinces = "เลือกจังหวัด", amphurs = "เลือกอำเภอ", thumbons = "เลือกตำบล";
    public String geographies_id = "", provinces_id = "", amphurs_id = "", thumbons_id = "";
    public String page_name = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        MyImageLoderSetting myImageLoderSetting = MyImageLoderSetting.getInatance(getApplicationContext());
        myImageLoderSetting.setting(getApplicationContext());

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (CustomViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setCurrentItem(1);
        mViewPager.setPagingEnabled(false);
        final TextView textViewTitle = (TextView) findViewById(R.id.textViewTittle);
        final TextView textViewName = (TextView) findViewById(R.id.textViewName2);

        final ImageView imageViewSearch = (ImageView) findViewById(R.id.imageViewSearch);
        imageViewSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(0);
            }
        });
        final ImageView imageViewAdd = (ImageView) findViewById(R.id.imageViewAdd);
        imageViewAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(2);
            }
        });

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {

            }


            @Override
            public void onPageSelected(int i) {
                if (i == 0) {
                    textViewTitle.setText("ค้นหาแหล่งท่องเที่ยว");
                    imageViewAdd.setVisibility(View.INVISIBLE);
                    imageViewSearch.setImageResource(R.drawable.ic_action_previous_item);
                    textViewName.setText("Back");
                    textViewName.setVisibility(View.VISIBLE);
                    textViewName.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(1);
                        }
                    });
                    imageViewSearch.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(1);
                        }
                    });
                } else if (i == 1) {
                   // mSectionsPagerAdapter.getItem(i).setUserVisibleHint(false);
                    mSectionsPagerAdapter.getItem(i).onResume();
                    textViewTitle.setText("แผนที่");
                    textViewName.setVisibility(View.INVISIBLE);
                    imageViewAdd.setVisibility(View.VISIBLE);
                    imageViewAdd.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(2);
                        }
                    });
                    imageViewSearch.setImageResource(R.drawable.ic_action_search);
                    imageViewSearch.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(0);
                        }
                    });

                } else if (i == 2) {
                    textViewTitle.setText("สำรวจแหล่งท่องเที่ยว");
                    imageViewAdd.setVisibility(View.INVISIBLE);
                    imageViewSearch.setImageResource(R.drawable.ic_action_previous_item);
                    textViewName.setText("Back");
                    textViewName.setVisibility(View.VISIBLE);
                    textViewName.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(1);
                        }
                    });
                    imageViewSearch.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(1);
                        }
                    });

                  //  mSectionsPagerAdapter.getItem(i).setUserVisibleHint(true);
                    mSectionsPagerAdapter.getItem(i).onResume();
                } else if (i == 3) {

                    textViewTitle.setText("เลือกประเภทแหล่งท่องเที่ยว");
                    imageViewAdd.setVisibility(View.INVISIBLE);
                    imageViewSearch.setImageResource(R.drawable.ic_action_previous_item);
                    textViewName.setText("Back");
                    textViewName.setVisibility(View.VISIBLE);
                    if (page_name.equalsIgnoreCase(SearchFragment.class.getSimpleName())) {
                        textViewName.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mViewPager.setCurrentItem(0);
                            }
                        });
                        imageViewSearch.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mViewPager.setCurrentItem(0);
                            }
                        });
                    } else if (page_name.equalsIgnoreCase(AddingFragment.class.getSimpleName())) {
                        textViewName.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mViewPager.setCurrentItem(2);
                            }
                        });
                        imageViewSearch.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mViewPager.setCurrentItem(2);
                            }
                        });
                    }


                } else if (i == 4) {
                    textViewTitle.setText("เลือกภูมิภาค");
                    imageViewAdd.setVisibility(View.INVISIBLE);
                    imageViewSearch.setImageResource(R.drawable.ic_action_previous_item);
                    textViewName.setText("Back");
                    textViewName.setVisibility(View.VISIBLE);
                    textViewName.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(2);
                        }
                    });
                    imageViewSearch.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(2);
                        }
                    });

                } else if (i == 5) {
                    textViewTitle.setText("เลือกจังหวัด");
                    imageViewAdd.setVisibility(View.INVISIBLE);
                    imageViewSearch.setImageResource(R.drawable.ic_action_previous_item);
                    textViewName.setText("Back");
                    textViewName.setVisibility(View.VISIBLE);
                    textViewName.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(2);
                        }
                    });
                    imageViewSearch.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(2);
                        }
                    });

                } else if (i == 6) {
                    textViewTitle.setText("เลือกอำเภอ");
                    imageViewAdd.setVisibility(View.INVISIBLE);
                    imageViewSearch.setImageResource(R.drawable.ic_action_previous_item);
                    textViewName.setText("Back");
                    textViewName.setVisibility(View.VISIBLE);
                    textViewName.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(2);
                        }
                    });
                    imageViewSearch.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(2);
                        }
                    });

                } else if (i == 7) {
                    textViewTitle.setText("เลือกตำบล");
                    imageViewAdd.setVisibility(View.INVISIBLE);
                    imageViewSearch.setImageResource(R.drawable.ic_action_previous_item);
                    textViewName.setText("Back");
                    textViewName.setVisibility(View.VISIBLE);
                    textViewName.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(2);
                        }
                    });
                    imageViewSearch.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(2);
                        }
                    });

                } else if (i == 8) {
                    textViewTitle.setText("Help");
                    imageViewAdd.setVisibility(View.INVISIBLE);
                    imageViewSearch.setImageResource(R.drawable.ic_action_previous_item);
                    textViewName.setText("Back");
                    textViewName.setVisibility(View.VISIBLE);
                    textViewName.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(2);
                        }
                    });
                    imageViewSearch.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(2);
                        }
                    });
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    @Override
    public void onBackPressed() {

        //   Log.i("onBackPressed()",mViewPager.getCurrentItem()+"");
        if (mViewPager.getCurrentItem() == 0) {
            mViewPager.setCurrentItem(1);
        } else if (mViewPager.getCurrentItem() == 1) {
            this.finish();
        } else if (mViewPager.getCurrentItem() == 2) {
            mViewPager.setCurrentItem(1);
        } else if (mViewPager.getCurrentItem() == 3) {
            mViewPager.setCurrentItem(0);
        } else if (mViewPager.getCurrentItem() == 4) {
            if (page_name.equalsIgnoreCase(SearchFragment.class.getSimpleName())) {
                mViewPager.setCurrentItem(0);
            } else if (page_name.equalsIgnoreCase(AddingFragment.class.getSimpleName())) {
                mViewPager.setCurrentItem(2);
            }

        } else if (mViewPager.getCurrentItem() == 5) {
            mViewPager.setCurrentItem(2);
        } else if (mViewPager.getCurrentItem() == 6) {
            mViewPager.setCurrentItem(2);
        } else if (mViewPager.getCurrentItem() == 7) {
            mViewPager.setCurrentItem(2);
        } else if (mViewPager.getCurrentItem() == 8) {
            mViewPager.setCurrentItem(1);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        ArrayList<Fragment> fragmentArrayList = new ArrayList<Fragment>();

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            //DetailFragment detailFragment = DetailFragment.newInstance("Detail", "data");
            SearchFragment searchFragment = SearchFragment.newInstance("Search", "data");
            MapFragment mapFragment = MapFragment.newInstance("Map", "data");
            AddingFragment addingFragment = AddingFragment.newInstance("Add", "data");

            fragmentArrayList.add(searchFragment);//0
            fragmentArrayList.add(mapFragment);//1
            fragmentArrayList.add(addingFragment);//2

            fragmentArrayList.add(AttractionTypesFragment.newInstance("", ""));//3
            fragmentArrayList.add(GeographiesFragment.newInstance("", ""));//4
            fragmentArrayList.add(ProvincesFragment.newInstance("", ""));//5

            fragmentArrayList.add(AmphursFragment.newInstance("", ""));//6
            fragmentArrayList.add(ThumbonsFragment.newInstance("", ""));//7
            fragmentArrayList.add(HelpFragment.newInstance("", ""));//8
            //fragmentArrayList.add(detailFragment);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return fragmentArrayList.get(position);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return fragmentArrayList.size();
        }


    }

//    /**
//     * A placeholder fragment containing a simple view.
//     */
//    public static class PlaceholderFragment extends Fragment {
//        /**
//         * The fragment argument representing the section number for this
//         * fragment.
//         */
//        private static final String ARG_SECTION_NUMBER = "section_number";
//
//        /**
//         * Returns a new instance of this fragment for the given section
//         * number.
//         */
//        public static PlaceholderFragment newInstance(int sectionNumber) {
//            PlaceholderFragment fragment = new PlaceholderFragment();
//            Bundle args = new Bundle();
//            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
//            fragment.setArguments(args);
//            return fragment;
//        }
//
//        public PlaceholderFragment() {
//        }
//
//        @Override
//        public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                                 Bundle savedInstanceState) {
//            View rootView = inflater.inflate(R.layout.fragment_map, container, false);
//            return rootView;
//        }
//    }

}
