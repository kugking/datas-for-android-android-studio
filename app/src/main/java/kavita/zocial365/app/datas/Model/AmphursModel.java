package kavita.zocial365.app.datas.Model;

/**
 * Created by kugking on 12/12/14 AD.
 */
public class AmphursModel {
    String id = "", amphur_id = "", amphur_name = "", province_id = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAmphur_id() {
        return amphur_id;
    }

    public void setAmphur_id(String amphur_id) {
        this.amphur_id = amphur_id;
    }

    public String getAmphur_name() {
        return amphur_name;
    }

    public void setAmphur_name(String amphur_name) {
        this.amphur_name = amphur_name;
    }

    public String getProvince_id() {
        return province_id;
    }

    public void setProvince_id(String province_id) {
        this.province_id = province_id;
    }
}
