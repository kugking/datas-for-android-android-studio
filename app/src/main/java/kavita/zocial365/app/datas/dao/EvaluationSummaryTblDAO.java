package kavita.zocial365.app.datas.dao;

/**
 * Created by kugking on 1/12/15 AD.
 */
public class EvaluationSummaryTblDAO extends BaseDao{
    @SuppressWarnings("tbl_tourist_eva_summary")
    private EvaluationSummaryDAO[] tbl_tourist_eva_summary;

    public EvaluationSummaryDAO[] getTbl_tourist_eva_summary() {
        return tbl_tourist_eva_summary;
    }

    public void setTbl_tourist_eva_summary(EvaluationSummaryDAO[] tbl_tourist_eva_summary) {
        this.tbl_tourist_eva_summary = tbl_tourist_eva_summary;
    }
}
