package kavita.zocial365.app.datas;

import android.app.Application;


import kavita.zocial365.app.datas.utils.Contextor;


public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Contextor.getInstance().init(getApplicationContext());
    }
}
