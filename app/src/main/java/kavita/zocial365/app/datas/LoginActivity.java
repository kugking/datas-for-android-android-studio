package kavita.zocial365.app.datas;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import org.json.JSONException;
import org.json.JSONObject;

import kavita.zocial365.app.datas.Model.UserModel;
import kavita.zocial365.app.datas.http.GetJson;


public class LoginActivity extends Activity {

    private EditText editText_username;
    private EditText editText_password;
    private String username = "username", spp_id = "spp_id", user_level_id = "user_level_id", name = "name", status = "status", create_date = "create_date", edit_date = "edit_date", auth_token = "auth_token";
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onResume() {
        super.onResume();
        NetworkConnection.isNetworkAvailable(LoginActivity.this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sharedPreferences = this.getSharedPreferences("kavita.zocial365.app.datas", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();


        final UserModel userModel = new UserModel();
        userModel.setUsername(sharedPreferences.getString(username, ""));
        userModel.setSpp_id(sharedPreferences.getString(spp_id, ""));
        userModel.setUser_level_id(sharedPreferences.getString(user_level_id, ""));
        userModel.setName(sharedPreferences.getString(name, ""));
        userModel.setStatus(sharedPreferences.getString(status, ""));
        userModel.setCreate_date(sharedPreferences.getString(create_date, ""));
        userModel.setEdit_date(sharedPreferences.getString(edit_date, ""));
        userModel.setAuth_token(sharedPreferences.getString(auth_token, ""));
        MySession.getInstance().setUserModel(userModel);
        if (userModel.getAuth_token().equals("") || userModel.getAuth_token() == "") {

        } else {
            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            finish();
            startActivity(intent);
        }
        Button button = (Button) findViewById(R.id.button_login);
        editText_username = (EditText) findViewById(R.id.editText_username);
        editText_password = (EditText) findViewById(R.id.editText_password);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NetworkConnection.isNetworkAvailable(LoginActivity.this);
                if (userModel.getAuth_token().equals("") || userModel.getAuth_token() == "") {
                    new Login().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, editText_username.getText().toString(), editText_password.getText().toString());
                } else {
                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    finish();
                    startActivity(intent);
                }

            }
        });
    }

    class Login extends AsyncTask<String, Void, Void> {
        String username = "username", spp_id = "spp_id", user_level_id = "user_level_id", name = "name", status = "status", create_date = "create_date", edit_date = "edit_date", auth_token = "auth_token";
        private String result;
        private ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setTitle("Downloading...");
            progressDialog.show();
        }

        /**
         * Override this method to perform a computation on a background thread. The
         * specified parameters are the parameters passed to {@link #execute}
         * by the caller of this task.
         * <p/>
         * This method can call {@link #publishProgress} to publish updates
         * on the UI thread.
         *
         * @param params The parameters of the task.
         * @return A result, defined by the subclass of this task.
         * @see #onPreExecute()
         * @see #onPostExecute
         * @see #publishProgress
         */


        @Override
        protected Void doInBackground(String... params) {
            String url = MyURL.HOST_NAME + "users/login.json?username=" + params[0] + "&password=" + params[1];
            GetJson getJson = new GetJson();
            result = getJson.getJSON(url);
            Log.i("Result", result);
            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.optString("status").equalsIgnoreCase("9200")) {
                    JSONObject jsonObject1 = jsonObject.optJSONObject("objects");
                    UserModel userModel = new UserModel();
                    userModel.setUsername(jsonObject1.optString(username));
                    userModel.setUsername(jsonObject1.optString(spp_id));
                    userModel.setUser_level_id(jsonObject1.optString(user_level_id));
                    userModel.setName(jsonObject1.optString(name));
                    userModel.setStatus(jsonObject1.optString(status));
                    userModel.setCreate_date(jsonObject1.optString(create_date));
                    userModel.setEdit_date(jsonObject1.optString(edit_date));
                    userModel.setAuth_token(jsonObject1.optString(auth_token));
                    MySession.getInstance().setUserModel(userModel);
                    Log.i("MySession name:", MySession.getInstance().getUserModel().getName() + " auth :" + MySession.getInstance().getUserModel().getAuth_token());

                    editor.putString(username, userModel.getUsername());
                    editor.putString(spp_id, userModel.getSpp_id());
                    editor.putString(user_level_id, userModel.getUser_level_id());
                    editor.putString(name, userModel.getName());
                    editor.putString(status, userModel.getStatus());
                    editor.putString(create_date, userModel.getCreate_date());
                    editor.putString(edit_date, userModel.getEdit_date());
                    editor.putString(auth_token, userModel.getAuth_token());
                    editor.commit();

                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    finish();
                    startActivity(intent);
                } else {
                    try {
                        MyAlertDialog.mAlertDialog(LoginActivity.this, jsonObject.getString("error") + "", "Sorry!!");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
