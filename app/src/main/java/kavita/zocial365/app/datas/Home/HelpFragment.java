package kavita.zocial365.app.datas.Home;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import kavita.zocial365.app.datas.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HelpFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HelpFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HelpFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private ArrayList<HashMap<String, Object>> arrayList = new ArrayList<HashMap<String, Object>>();
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HelpFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HelpFragment newInstance(String param1, String param2) {
        HelpFragment fragment = new HelpFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public HelpFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        HashMap<String, Object> dataHelp = new HashMap<String, Object>();
        dataHelp.put("image", R.drawable.ic_action_search);
        dataHelp.put("text", "ปุ่มสำหรับเปิดค้นหาแหล่งท่องเที่ยว");
        arrayList.add(dataHelp);
        dataHelp = new HashMap<String, Object>();
        dataHelp.put("image", R.drawable.ic_action_new);
        dataHelp.put("text", "ปุ่มสำหรับเปิดหน้าเพิ่มแหล่งท่องเที่ยว");
        arrayList.add(dataHelp);
        dataHelp = new HashMap<String, Object>();
        dataHelp.put("image", R.drawable.ic_action_location_found);
        dataHelp.put("text", "ปุ่มสำหรับเปิดค้นหาแหล่งท่องเที่ยว");
        arrayList.add(dataHelp);
        dataHelp = new HashMap<String, Object>();
        dataHelp.put("image", R.drawable.icon_logout);
        dataHelp.put("text", "ปุ่มสำหรับลงชื่อออก (Logout)");
        arrayList.add(dataHelp);
        dataHelp = new HashMap<String, Object>();
        dataHelp.put("image", R.drawable.gallery_image);
        dataHelp.put("text", "ปุ่มสำหรับเพิ่มรูปใหม่");
        arrayList.add(dataHelp);
        dataHelp = new HashMap<String, Object>();
        dataHelp.put("image", R.drawable.ic_action_discard);
        dataHelp.put("text", "ปุ่มสำหรับลบภาพแหล่งท่องเที่ยว");
        arrayList.add(dataHelp);
        dataHelp = new HashMap<String, Object>();
        dataHelp.put("image", R.drawable.ic_action_paste);
        dataHelp.put("text", "ปุ่มสำหรับเปิดหน้าการให้คะแนนประเมิน");
        arrayList.add(dataHelp);
        dataHelp = new HashMap<String, Object>();
        dataHelp.put("image", R.drawable.ic_action_view_as_list);
        dataHelp.put("text", "ปุ่มสำหรับดูการประเมินแต่ละปี");
        arrayList.add(dataHelp);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_help, container, false);
        ListView listView = (ListView) view.findViewById(R.id.listView7);
        HelpAdapter helpAdapter = new HelpAdapter();
        listView.setAdapter(helpAdapter);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    class HelpAdapter extends BaseAdapter {

        /**
         * How many items are in the data set represented by this Adapter.
         *
         * @return Count of items.
         */
        @Override
        public int getCount() {
            return arrayList.size();
        }

        /**
         * Get the data item associated with the specified position in the data set.
         *
         * @param position Position of the item whose data we want within the adapter's
         *                 data set.
         * @return The data at the specified position.
         */
        @Override
        public Object getItem(int position) {
            return null;
        }

        /**
         * Get the row id associated with the specified position in the list.
         *
         * @param position The position of the item within the adapter's data set whose row id we want.
         * @return The id of the item at the specified position.
         */
        @Override
        public long getItemId(int position) {
            return 0;
        }

        /**
         * Get a View that displays the data at the specified position in the data set. You can either
         * create a View manually or inflate it from an XML layout file. When the View is inflated, the
         * parent View (GridView, ListView...) will apply default layout parameters unless you use
         * {@link android.view.LayoutInflater#inflate(int, android.view.ViewGroup, boolean)}
         * to specify a root view and to prevent attachment to the root.
         *
         * @param position    The position of the item within the adapter's data set of the item whose view
         *                    we want.
         * @param convertView The old view to reuse, if possible. Note: You should check that this view
         *                    is non-null and of an appropriate type before using. If it is not possible to convert
         *                    this view to display the correct data, this method can create a new view.
         *                    Heterogeneous lists can specify their number of view types, so that this View is
         *                    always of the right type (see {@link #getViewTypeCount()} and
         *                    {@link #getItemViewType(int)}).
         * @param parent      The parent that this view will eventually be attached to
         * @return A View corresponding to the data at the specified position.
         */
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            Holder holder = null;
            if (view != null) {
                holder = (Holder) view.getTag();
            } else {
                view = LayoutInflater.from(getActivity()).inflate(R.layout.layout_item_help, null, false);
                holder = new Holder();
                view.setTag(holder);
            }
            holder.imageView = (ImageView) view.findViewById(R.id.imageView9);
            holder.textView = (TextView) view.findViewById(R.id.textView_help);

            holder.imageView.setImageResource((Integer) arrayList.get(position).get("image"));
            holder.textView.setText((String) arrayList.get(position).get("text"));
            return view;
        }
    }

    class Holder {
        ImageView imageView;
        TextView textView;
    }
}
