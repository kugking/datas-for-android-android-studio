package kavita.zocial365.app.datas.dao;

/**
 * Created by kugking on 1/12/15 AD.
 */
public class EvaluationSummaryDAO extends BaseDao{
    @SuppressWarnings("tour_id") private String tour_id;
    @SuppressWarnings("year") private String year;
    @SuppressWarnings("name") private String name;
    @SuppressWarnings("outstanding") private String outstanding;
    @SuppressWarnings("service_activity") private String service_activity;
    @SuppressWarnings("facility") private String facility;
    @SuppressWarnings("security") private String security;
    @SuppressWarnings("management") private String management;
    @SuppressWarnings("social_useful") private String social_useful;
    @SuppressWarnings("sum_score") private String sum_score;
    @SuppressWarnings("lev_score") private String lev_score;

    public String getTour_id() {
        return tour_id;
    }

    public void setTour_id(String tour_id) {
        this.tour_id = tour_id;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOutstanding() {
        return outstanding;
    }

    public void setOutstanding(String outstanding) {
        this.outstanding = outstanding;
    }

    public String getService_activity() {
        return service_activity;
    }

    public void setService_activity(String service_activity) {
        this.service_activity = service_activity;
    }

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public String getSecurity() {
        return security;
    }

    public void setSecurity(String security) {
        this.security = security;
    }

    public String getManagement() {
        return management;
    }

    public void setManagement(String management) {
        this.management = management;
    }

    public String getSocial_useful() {
        return social_useful;
    }

    public void setSocial_useful(String social_useful) {
        this.social_useful = social_useful;
    }

    public String getSum_score() {
        return sum_score;
    }

    public void setSum_score(String sum_score) {
        this.sum_score = sum_score;
    }

    public String getLev_score() {
        return lev_score;
    }

    public void setLev_score(String lev_score) {
        this.lev_score = lev_score;
    }
}
