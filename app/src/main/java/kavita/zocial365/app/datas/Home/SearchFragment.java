package kavita.zocial365.app.datas.Home;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import kavita.zocial365.app.datas.MySession;
import kavita.zocial365.app.datas.MyURL;
import kavita.zocial365.app.datas.ViewPager.CustomViewPager;
import kavita.zocial365.app.datas.DetailActivity;
import kavita.zocial365.app.datas.HomeActivity;
import kavita.zocial365.app.datas.Model.ImagesModel;
import kavita.zocial365.app.datas.Model.TouristModel;
import kavita.zocial365.app.datas.R;
import kavita.zocial365.app.datas.http.GetJson;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SearchFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private ArrayList<TouristModel> touristModels = new ArrayList<TouristModel>();
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private CustomViewPager mViewPager;
    private HomeActivity.SectionsPagerAdapter mSectionsPagerAdapter;
    private OnFragmentInteractionListener mListener;
    private SearchServer searchServer;
    private ImageListViewAdapter imageListViewAdapter;
    private int limit = 15;
    private Bitmap bitmapDefalt;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SearchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SearchFragment newInstance(String param1, String param2) {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        touristModels = new ArrayList<TouristModel>();
        mViewPager = ((HomeActivity) getActivity()).mViewPager;
        mSectionsPagerAdapter = ((HomeActivity) getActivity()).mSectionsPagerAdapter;
        bitmapDefalt = BitmapFactory.decodeResource(getResources(), R.drawable.ic_action_picture);
        Log.i("SearchFragment", "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i("SearchFragment", "onCreateView");
        View view = (View) inflater.inflate(R.layout.fragment_search, container, false);

        TextView textViewType = (TextView) view.findViewById(R.id.textViewType);

        ListView listView = (ListView) view.findViewById(R.id.listView1);
        TextView textViewSearch = (TextView) view.findViewById(R.id.textView2);


        textViewType.setText(((HomeActivity) getActivity()).type_name + "");


        imageListViewAdapter = new ImageListViewAdapter(getActivity());
        listView.setAdapter(imageListViewAdapter);

        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.layout_type);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity) getActivity()).page_name = SearchFragment.class.getSimpleName();
                mViewPager.setCurrentItem(3);
            }
        });
        final SearchView searchView = (SearchView) view.findViewById(R.id.searchView1);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                onSearchServer(query.replace(" ", ""));
                //imageListViewAdapter.notifyDataSetChanged();

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                onSearchServer(newText.replace(" ", ""));
                // imageListViewAdapter.notifyDataSetChanged();
                return true;
            }
        });
//        textViewSearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getActivity(), DetailActivity.class);
//                startActivity(intent);
//            }
//        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), DetailActivity.class);
//                Log.i("ID", "id:" + touristModels.get(position).getId());
                String i = touristModels.get(position).getId() + "";
                intent.putExtra("id", i + "");
                intent.putExtra("title", touristModels.get(position).getName());
                intent.putExtra("tour_id", touristModels.get(position).getTour_id());
                startActivity(intent);
            }
        });

        new SearchServerUpdate("").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        return view;
    }

    public void onSearchServer(String key) {
        if (searchServer == null) {
            searchServer = (SearchServer) new SearchServer(key).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
        } else {
            searchServer.cancel(true);
            searchServer = (SearchServer) new SearchServer(key).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    public class ImageListViewAdapter extends BaseAdapter {
        Activity activity;


        public ImageListViewAdapter(Activity activity) {
            this.activity = activity;
            //touristModels = new ArrayList<String>();
        }

        @Override
        public int getCount() {
            return touristModels.size();
        }

        @Override
        public Object getItem(int position) {
            return touristModels.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder holder = null;
            View view = convertView;
//            TextView textView = null;
//            ImageView imageView = null;
            //new DownloadImage(touristModels.get(position).getId() + "", position).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            if (view == null) {
                holder = new Holder();
                view = LayoutInflater.from(activity).inflate(R.layout.layout_search_item, parent, false);
                holder.textView = (TextView) view.findViewById(R.id.textViewItemName);
                holder.imageView = (ImageView) view.findViewById(R.id.imageView7);
                view.setTag(holder);
            } else {
                holder = (Holder) view.getTag();
            }

            try {
                com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
                imageLoader.displayImage(touristModels.get(position).getImagesModels().get(0).getImage_thumb_url() + "", holder.imageView);
            } catch (Exception e) {
                e.printStackTrace();
                holder.imageView.setImageBitmap(bitmapDefalt);
            }
            try {
                holder.textView.setText(touristModels.get(position).getName());
            } catch (Exception e) {
                e.printStackTrace();
            }

            // new DownloadImage(touristModels.get(position).getId() + "", position, holder.imageView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//            try {
//
//                new ImageLoader(holder.imageView,
//                        "http://tis.dasta.or.th/dasta_survey/tourist_profile_img/" + touristModels.get(position).getImagesModels().get(0).getPath())
//                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//            } catch (Exception e) {
//                e.printStackTrace();
//
//            }

            return view;
        }
    }

    class Holder {
        TextView textView;
        ImageView imageView;
    }

    class ImageLoader extends AsyncTask<Void, Void, Boolean> {
        Bitmap bitmap;
        String url;
        ImageView imageView;
        com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();

        ImageLoader(ImageView imageView, String url) {
            this.imageView = imageView;
            this.url = url;

        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                bitmap = imageLoader.loadImageSync(url);
                return true;
            } catch (Exception e) {
                e.printStackTrace();

            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean) {
                imageView.setImageBitmap(bitmap);
            }
        }
    }

    class SearchServer extends AsyncTask<Void, Void, Boolean> {
        String key = "";

        SearchServer(String key) {
            this.key = key;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // touristModels.clear();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            GetJson getJson = new GetJson();
            String url = MyURL.HOST_NAME + "tbl_tourist_ups.json?q[name_cont]=" + key + "&q[type_id_eq]="
                    + ((HomeActivity) getActivity()).type + "&limit=" + limit
                    + "&auth_token=" + MySession.getInstance().getUserModel().getAuth_token();
            //  Log.i("Search URL", "url :" + url);
            String result = getJson.getJSON(url);
            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.optString("status", "").equalsIgnoreCase("9200")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("objects");

                    touristModels = addTouristModel(jsonArray);
                    return true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {

            super.onPostExecute(aBoolean);
            if (aBoolean) {
                imageListViewAdapter.notifyDataSetChanged();
            }
        }
    }

    class SearchServerUpdate extends AsyncTask<Void, Void, Boolean> {
        String key = "";

        SearchServerUpdate(String key) {
            this.key = key;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //touristModels.clear();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            GetJson getJson = new GetJson();
            String url = MyURL.HOST_NAME + "tbl_tourist_ups.json" + "?auth_token=" + MySession.getInstance().getUserModel().getAuth_token();
            ;
            //  Log.i("Search URL", "url :" + url);
            String result = getJson.getJSON(url);
            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.optString("status", "").equalsIgnoreCase("9200")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("objects");

                    touristModels = addTouristModel(jsonArray);
                    return true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {

            super.onPostExecute(aBoolean);
            if (aBoolean) {
                imageListViewAdapter.notifyDataSetChanged();
            }
        }
    }

    class DownloadImage extends AsyncTask<Void, Void, Boolean> {
        String img_id = "";
        int position = 0;
        ImageView imageView;

        DownloadImage(String img_id, int posotion, ImageView imageView) {
            this.img_id = img_id;
            this.position = posotion;
            this.imageView = imageView;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            GetJson getJson = new GetJson();
            String url = MyURL.HOST_NAME + "tbl_tourist_images.json?q[tour_id_eq]=" + img_id +
                    "&limit=" + limit + "&auth_token=" + MySession.getInstance().getUserModel().getAuth_token();
            //  Log.i("Image URL", "url :" + url);
            String result = getJson.getJSON(url);
            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.optString("status", "").equalsIgnoreCase("9200")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("objects");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.optJSONObject(i);
//                        touristModels.get(position).setId_img(jsonObject1.optString("img_id"));
//                        touristModels.get(position).setPath_img(jsonObject1.optString("path"));
//                        touristModels.get(position).setId_img(jsonObject1.optString("id"));
                        ImagesModel imagesModel = new ImagesModel();
                        imagesModel.setId(jsonObject1.optString("id"));
                        imagesModel.setImg_id(jsonObject1.optString("img_id"));
                        imagesModel.setPath(jsonObject1.optString("path"));
                        imagesModel.setTour_id(jsonObject1.optString("tour_id"));
                        touristModels.get(position).addImageModel(imagesModel);
                        new ImageLoader(imageView, "http://tis.dasta.or.th/dasta_survey/tourist_profile_img/" + imagesModel.getPath()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }

                    return true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return true;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);

        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

        }
    }

    public ArrayList<TouristModel> addTouristModel(JSONArray jsonArray) {
        ArrayList<TouristModel> touristModels = new ArrayList<TouristModel>();
        String tour_id = "tour_id", name = "name", name_form = "name_form", care_agen = "care_agen", date_create = "date_create", type_id = "type_id", type_type_nm_t = "type_type_nm_t",
                sec_type_nm_t = "sec_type_nm_t", type_sub_nm_t = "type_sub_nm_t", description = "description",
                tour_day = "tour_day", time_open = "time_open", time_close = "time_close",
                addr = "addr", road = "road", thumbon_id = "thumbon_id", amphur_id = "amphur_id", province_id = "province_id", region_id = "region_id", zipcode = "zipcode",
                tel = "tes", fax = "fax", website = "website", own_land = "own_land", own_land_right = "own_land_right",
                lot_discribe = "lot_discribe", manage_agen = "manage_agen", loc_or_no1 = "loc_or_no1", th_tour_dialy = "th_tour_dialy",
                status = "status", dasta_id = "dasta_id", status_low_carbon = "status_low_carbon", edit_date = "edit_date", user_id = "user_id", id = "id", fee_thai_child = "fee_thai_child",
                fee_thai_adult = "fee_thai_adult", fee_forient_child = "fee_forient_child", fee_forient_adult = "fee_forient_adult", degreex = "degreex", degreey = "degreey",
                th_tour_monthly = "th_tour_monthly", th_tour_yearly = "th_tour_yearly", for_tour_daily = "for_tour_daily",
                for_tour_monthly = "for_tour_monthly", for_tour_yearly = "for_tour_yearly", gid = "gid", tbl_tourist_image = "tbl_tourist_image";
        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject jsonObject = jsonArray.optJSONObject(i);

//            if(jsonObject.optString(id).equalsIgnoreCase(touristModels.get()))
            TouristModel touristModel = new TouristModel();
            touristModel.setId(jsonObject.optInt(id));
            touristModel.setTour_id(jsonObject.optString(tour_id));
            touristModel.setName(jsonObject.optString(name));
            touristModel.setName_form(jsonObject.optString(name_form));
            touristModel.setCare_agen(jsonObject.optString(care_agen));
            touristModel.setDate_create(jsonObject.optString(date_create));
            touristModel.setType_id(jsonObject.optString(type_id));
            touristModel.setType_type_nm_t(jsonObject.optString(type_type_nm_t));
            touristModel.setSec_type_nm_t(jsonObject.optString(sec_type_nm_t));
            touristModel.setType_sub_nm_t(jsonObject.optString(type_sub_nm_t));
            touristModel.setDescription(jsonObject.optString(description));
            touristModel.setTour_day(jsonObject.optString(tour_day));

            touristModel.setTime_open(jsonObject.optString(time_open));
            touristModel.setTime_close(jsonObject.optString(time_close));

            touristModel.setFee_thai_child(jsonObject.optInt(fee_thai_child));
            touristModel.setFee_thai_adult(jsonObject.optInt(fee_thai_adult));
            touristModel.setFee_forient_child(jsonObject.optInt(fee_forient_child));
            touristModel.setFee_forient_adult(jsonObject.optInt(fee_forient_adult));
            touristModel.setDegreex(jsonObject.optDouble(degreex));
            touristModel.setDegreey(jsonObject.optDouble(degreey));
            touristModel.setAddr(jsonObject.optString(addr));
            touristModel.setRoad(jsonObject.optString(road));
            touristModel.setThumbon_id(jsonObject.optString(thumbon_id));
            touristModel.setAmphur_id(jsonObject.optString(amphur_id));
            touristModel.setProvince_id(jsonObject.optString(province_id));
            touristModel.setRegion_id(jsonObject.optString(region_id));
            touristModel.setZipcode(jsonObject.optString(zipcode));
            touristModel.setTel(jsonObject.optString(tel));
            touristModel.setFax(jsonObject.optString(fax));
            touristModel.setWebsite(jsonObject.optString(website));
            touristModel.setOwn_land(jsonObject.optString(own_land));
            touristModel.setOwn_land_right(jsonObject.optString(own_land_right));
            touristModel.setLot_discribe(jsonObject.optString(lot_discribe));
            touristModel.setManage_agen(jsonObject.optString(manage_agen));
            touristModel.setLoc_or_no1(jsonObject.optString(loc_or_no1));
            touristModel.setTh_tour_dialy(jsonObject.optInt(th_tour_dialy));
            touristModel.setTh_tour_monthly(jsonObject.optInt(th_tour_monthly));
            touristModel.setTh_tour_yearly(jsonObject.optInt(th_tour_yearly));
            touristModel.setFor_tour_daily(jsonObject.optInt(for_tour_daily));
            touristModel.setFor_tour_monthly(jsonObject.optInt(for_tour_monthly));
            touristModel.setFor_tour_yearly(jsonObject.optInt(for_tour_yearly));
            touristModel.setStatus(jsonObject.optString(status));
            touristModel.setGid(jsonObject.optInt(gid));
            touristModel.setDasta_id(jsonObject.optString(dasta_id));
            touristModel.setStatus_low_carbon(jsonObject.optString(status_low_carbon));
            touristModel.setEdit_date(jsonObject.optString(edit_date));
            touristModel.setUser_id(jsonObject.optString(user_id));
            JSONArray jsonArray2 = jsonObject.optJSONArray(tbl_tourist_image);
            for (int p = 0; p < jsonArray2.length(); p++) {
                JSONObject jsonArray1 = jsonArray2.optJSONObject(p);
                ImagesModel imagesModel = new ImagesModel();

                imagesModel.setId(jsonArray1.optString("id"));
                imagesModel.setImg_id(jsonArray1.optString("img_id"));
                imagesModel.setTour_id(jsonArray1.optString("tour_id"));
                imagesModel.setPath(jsonArray1.optString("path"));
                imagesModel.setImage_url(jsonArray1.optString("image_url"));
                imagesModel.setImage_thumb_url(jsonArray1.optString("image_thumb_url"));

                touristModel.addImageModel(imagesModel);
            }
            touristModels.add(touristModel);

        }
        return touristModels;
    }

}
