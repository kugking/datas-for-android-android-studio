package kavita.zocial365.app.datas;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import kavita.zocial365.app.datas.Detail.AmphursFragment;
import kavita.zocial365.app.datas.Detail.AssessmentFragment;
import kavita.zocial365.app.datas.Detail.AttractionTypesFragment;
import kavita.zocial365.app.datas.Detail.DetailFragment;
import kavita.zocial365.app.datas.Detail.EditDetailFragment;
import kavita.zocial365.app.datas.Detail.EvaluateFragment;
import kavita.zocial365.app.datas.Detail.GeographiesFragment;
import kavita.zocial365.app.datas.Detail.ImagesFragment;
import kavita.zocial365.app.datas.Detail.ProvincesFragment;
import kavita.zocial365.app.datas.Detail.ThumbonsFragment;
import kavita.zocial365.app.datas.Model.TouristModel;
import kavita.zocial365.app.datas.ViewPager.CustomViewPager;


public class DetailActivity extends FragmentActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link android.support.v13.app.FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v13.app.FragmentStatePagerAdapter}.
     */
    public SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link android.support.v4.view.ViewPager} that will host the section contents.
     */
    public CustomViewPager mViewPager;
    public String id = "", tour_id = "";
    public TextView textViewEdit;
    public TextView textViewTitle;
    public TextView textViewName;
    public ImageView imageView;
    public String title = "";
    public String type = "";
    public String type_name = "เลือกประเภท";
    public String geographies = "เลือกภาค", provinces = "เลือกจังหวัด", amphurs = "เลือกอำเภอ", thumbons = "เลือกตำบล";
    public String geographies_id = "", provinces_id = "", amphurs_id = "", thumbons_id = "";
    public String page_name = "";
    public TouristModel touristModel = new TouristModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Intent intent = this.getIntent();
        Bundle extras = intent.getExtras();
        id = extras.getString("id")+"";
        title = extras.getString("title");
        tour_id = extras.getString("tour_id");
        //  Log.i("ID", "id:" + id);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());

        imageView = (ImageView) findViewById(R.id.imageViewBack);
        textViewName = (TextView) findViewById(R.id.textViewName);
        textViewTitle = (TextView) findViewById(R.id.textViewTittleImage);
        textViewEdit = (TextView) findViewById(R.id.textViewEdit2);
        // Set up the ViewPager with the sections adapter.
        mViewPager = (CustomViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setCurrentItem(1);
        mViewPager.setPagingEnabled(false);
        textViewTitle.setText(title);
        textViewName.setText("ค้นหา");
        textViewEdit.setText("แก้ไข");
        textViewEdit.setVisibility(View.VISIBLE);
        textViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(4);
            }
        });
        textViewName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {

            }

            @Override
            public void onPageSelected(int i) {
                mSectionsPagerAdapter.notifyDataSetChanged();
                if (i == 1) {
                    textViewTitle.setText(title);
                    textViewName.setText("Back");
                    textViewEdit.setText("แก้ไข");
                    textViewEdit.setVisibility(View.VISIBLE);
                    textViewName.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    });
                    textViewEdit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(4);
                        }
                    });
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    });

                } else if (i == 2) {
                    textViewTitle.setText("ผลประเมินแหล่งท่องเที่ยว");
                    textViewName.setText("Back");
                    textViewEdit.setVisibility(View.INVISIBLE);
                    textViewName.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(1);
                        }
                    });
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(1);
                        }
                    });
                } else if (i == 3) {
                    mSectionsPagerAdapter.getItem(i).setUserVisibleHint(true);
                    mSectionsPagerAdapter.getItem(i).onResume();
                    textViewTitle.setText("รูปภาพ");
                    textViewName.setText(title);
                    textViewEdit.setVisibility(View.INVISIBLE);
                    textViewName.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(1);
                        }
                    });
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(1);
                        }
                    });
                } else if (i == 0) {
                    textViewTitle.setText("แบบประเมินแหล่งท่องเที่ยว");
                    textViewName.setText("Back");
                    textViewEdit.setVisibility(View.INVISIBLE);
                    textViewName.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(1);
                        }
                    });
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(1);
                        }
                    });
                } else if (i == 4) {
                    mSectionsPagerAdapter.getItem(i).setUserVisibleHint(true);
                    mSectionsPagerAdapter.getItem(i).onResume();
                    textViewTitle.setText("แก้ไขข้อมูล:" + title);
                    textViewName.setText("Back");
                    textViewEdit.setVisibility(View.INVISIBLE);
                    textViewName.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(1);
                        }
                    });
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(1);
                        }
                    });
                } else if (i == 5) {
                    textViewName.setText("Back");
                    textViewTitle.setText("เลือกประเภทแหล่งท่องเที่ยว");
                    textViewEdit.setVisibility(View.INVISIBLE);
                    textViewName.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(4);
                        }
                    });
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(4);
                        }
                    });
                } else if (i == 6) {
                    textViewName.setText("Back");
                    textViewTitle.setText("เลือกภูมิภาค");
                    textViewEdit.setVisibility(View.INVISIBLE);
                    textViewName.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(4);
                        }
                    });
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(4);
                        }
                    });
                } else if (i == 7) {
                    textViewName.setText("Back");
                    textViewEdit.setVisibility(View.INVISIBLE);
                    textViewTitle.setText("เลือกจังหวัด");
                    textViewName.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(4);
                        }
                    });
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(4);
                        }
                    });
                } else if (i == 8) {
                    textViewName.setText("Back");
                    textViewEdit.setVisibility(View.INVISIBLE);
                    textViewTitle.setText("เลือกอำเภอ");
                    textViewName.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(4);
                        }
                    });
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(4);
                        }
                    });
                } else if (i == 9) {
                    textViewName.setText("Back");
                    textViewTitle.setText("เลือกตำบล");
                    textViewEdit.setVisibility(View.INVISIBLE);
                    textViewName.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(4);
                        }
                    });
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(4);
                        }
                    });
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_image, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        ArrayList<Fragment> fragmentArrayList = new ArrayList<Fragment>();

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            fragmentArrayList.add(EvaluateFragment.newInstance("", ""));//0
            fragmentArrayList.add(DetailFragment.newInstance(id, tour_id));//1
            fragmentArrayList.add(AssessmentFragment.newInstance("", ""));//2
            fragmentArrayList.add(ImagesFragment.newInstance(tour_id, ""));//3
            fragmentArrayList.add(EditDetailFragment.newInstance("", ""));//4

            fragmentArrayList.add(AttractionTypesFragment.newInstance("", ""));//5
            fragmentArrayList.add(GeographiesFragment.newInstance("", ""));//6
            fragmentArrayList.add(ProvincesFragment.newInstance("", ""));//7
            fragmentArrayList.add(AmphursFragment.newInstance("", ""));//8
            fragmentArrayList.add(ThumbonsFragment.newInstance("", ""));//9
            // fragmentArrayList.add(new ImageFragment());
        }

        public void add(Fragment fragment) {
            fragmentArrayList.add(fragment);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return fragmentArrayList.get(position);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return fragmentArrayList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_section1).toUpperCase(l);
                case 1:
                    return getString(R.string.title_section2).toUpperCase(l);
                case 2:
                    return getString(R.string.title_section3).toUpperCase(l);
            }
            return null;
        }
    }

    @Override
    public void onBackPressed() {
        if (mViewPager.getCurrentItem() == 1) {
            this.finish();
        } else if (mViewPager.getCurrentItem() == 0) {
            mViewPager.setCurrentItem(1);
        } else if (mViewPager.getCurrentItem() == 2) {
            mViewPager.setCurrentItem(1);
        } else if (mViewPager.getCurrentItem() == 3) {
            mViewPager.setCurrentItem(1);
        } else if (mViewPager.getCurrentItem() == 4) {
            mViewPager.setCurrentItem(1);
        } else if (mViewPager.getCurrentItem() == 5) {
            mViewPager.setCurrentItem(4);
        } else if (mViewPager.getCurrentItem() == 6) {
            mViewPager.setCurrentItem(4);
        } else if (mViewPager.getCurrentItem() == 7) {
            mViewPager.setCurrentItem(4);
        } else if (mViewPager.getCurrentItem() == 8) {
            mViewPager.setCurrentItem(4);
        } else if (mViewPager.getCurrentItem() == 9) {
            mViewPager.setCurrentItem(4);
        }
    }


}
