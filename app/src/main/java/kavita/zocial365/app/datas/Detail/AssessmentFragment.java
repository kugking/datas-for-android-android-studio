package kavita.zocial365.app.datas.Detail;

import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.PointLabelFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;
import com.androidplot.xy.XYStepMode;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.Arrays;
import java.util.Comparator;

import kavita.zocial365.app.datas.DetailActivity;
import kavita.zocial365.app.datas.MySession;
import kavita.zocial365.app.datas.MyURL;
import kavita.zocial365.app.datas.R;
import kavita.zocial365.app.datas.dao.EvaluationSummaryCollectionsDAO;
import kavita.zocial365.app.datas.dao.EvaluationSummaryDAO;
import kavita.zocial365.app.datas.http.HTTPEngine;
import kavita.zocial365.app.datas.http.HTTPEngineListener;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AssessmentFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AssessmentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AssessmentFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String url;
    private OnFragmentInteractionListener mListener;
    private XYPlot xyPlot;
    String[] years;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AssessmentFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AssessmentFragment newInstance(String param1, String param2) {
        AssessmentFragment fragment = new AssessmentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public AssessmentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

        }
        Log.i("AssessmentFragment", "onCreate");
    }

    @Override
    public void onResume() {
        super.onResume();
        new Thread(new Runnable() {
            @Override
            public void run() {
                loadData();
            }
        }).start();

        Log.i("AssessmentFragment", "onResume");
    }

    public void loadData() {
        url = MyURL.HOST_NAME + "tbl_tourist_ups/" + ((DetailActivity) getActivity()).tour_id + ".json?auth_token=" + MySession.getInstance().getUserModel().getAuth_token();
        Log.i("loadData", url);

        HTTPEngine.getInstance().loadEvaluation(new HTTPEngineListener<EvaluationSummaryCollectionsDAO>() {

            private Number[] scores;

            @Override
            public void onResponse(EvaluationSummaryCollectionsDAO data, String rawData) {
                Arrays.sort(data.getObjects().getTbl_tourist_eva_summary(), new Comparator<EvaluationSummaryDAO>() {

                    @Override
                    public int compare(EvaluationSummaryDAO lhs, EvaluationSummaryDAO rhs) {
                        return Integer.valueOf(lhs.getYear()).compareTo(Integer.valueOf(rhs.getYear()));
                    }
                });
                scores = new Number[data.getObjects().getTbl_tourist_eva_summary().length + 1];

                for (int i = 0; i < scores.length; i++) {
                    int index = i;
                    if (index == 0) {
                        Log.i("onResponse 0", "i =" + i + "," + (index) + "");
                        scores[index] = 0;
                    } else {
                        Log.i("onResponse", "i =" + i + "," + (index) + "");
                        scores[index] = Double.valueOf(data.getObjects().getTbl_tourist_eva_summary()[i - 1].getSum_score());
                    }


                }
                years = new String[data.getObjects().getTbl_tourist_eva_summary().length ];

                for (int i = 0; i < years.length; i++) {
//                    if (i == 0) {
//                        years[i] = "";
//                    } else {
                        years[i] = data.getObjects().getTbl_tourist_eva_summary()[i ].getYear();
                  //  }

                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // Converting the above income array into XYSeries
                        XYSeries incomeSeries = new SimpleXYSeries(
                                Arrays.asList(scores),                   // array => list
                                SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, // Y_VALS_ONLY means use the element index as the x value
                                "คะแนนการประเมิน");
                        LineAndPointFormatter series1Format = new LineAndPointFormatter();
                        series1Format.setPointLabelFormatter(new PointLabelFormatter());
                        series1Format.configure(getActivity(),
                                R.xml.line_point_formatter_with_plf);
                        xyPlot.addSeries(incomeSeries, series1Format);
                        xyPlot.setDomainValueFormat(new Format() {

                            @Override
                            public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {

                                return new StringBuffer(years[((Number) obj).intValue()]);
                            }

                            @Override
                            public Object parseObject(String source, ParsePosition pos) {
                                return null;
                            }
                        });
                        xyPlot.setTicksPerRangeLabel(1);
                        xyPlot.setTicksPerDomainLabel(years.length);
                    }
                });


            }

            @Override
            public void onFailure(EvaluationSummaryCollectionsDAO data, String rawData) {
                Log.i("onResponse1", rawData);
            }
        }, url);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_assessment, container, false);
        xyPlot = (XYPlot) view.findViewById(R.id.plot);
        TextView textView = (TextView) view.findViewById(R.id.textViewTitle);
        textView.setText(((DetailActivity) getActivity()).title);
        Log.i("AssessmentFragment", "onCreateView");
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
