package kavita.zocial365.app.datas.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;

import android.util.Log;

public class PostJSON {
    StringBuilder str = new StringBuilder();
    HttpClient httpclient = null;
    HttpPost httppost = null;

    public PostJSON() {
        // TODO Auto-generated constructor stub
    }

    // http://www.thaicreate.com/mobile/android-httpget-httppost.html
    public String postJSON(String URL, List<NameValuePair> map) {
        Log.d("Post log", "URL :" + URL + " , Data: " + map.toString());

        httppost = new HttpPost(URL);
        httpclient = new DefaultHttpClient();
        try {
            UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(map, "UTF-8");
            urlEncodedFormEntity.setContentEncoding(HTTP.UTF_8);

            httppost.setEntity(urlEncodedFormEntity);

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            HttpResponse response = httpclient.execute(httppost);

            StatusLine statusLine = response.getStatusLine();

            int statusCode = statusLine.getStatusCode();

            if (statusCode != 0) { // Status OK

                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                    Log.d("Setting write", str.toString());
                    // Log.d("setting", ""+line);
                }

            } else {
                Log.e("post ERROR", "Failed to download result..");
            }

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return str.toString();

    }

    public String postMultipart(String URL, MultipartEntity map) {

        Log.d("Post log", "URL :" + URL);

        httppost = new HttpPost(URL);
        httpclient = new DefaultHttpClient();
        httppost.setEntity(map);

        try {
            HttpResponse response = httpclient.execute(httppost);

            StatusLine statusLine = response.getStatusLine();

            int statusCode = statusLine.getStatusCode();

            if (statusCode != 0) { // Status OK

                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                    Log.d("Setting write", str.toString());
                    // Log.d("setting", ""+line);
                }

            } else {
                Log.e("post ERROR", "Failed to download result..");
            }

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return str.toString();

    }
}
