package kavita.zocial365.app.datas.Detail;

import android.app.Fragment;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import kavita.zocial365.app.datas.DetailActivity;
import kavita.zocial365.app.datas.Home.AddingFragment;
import kavita.zocial365.app.datas.Home.SearchFragment;
import kavita.zocial365.app.datas.Model.AttractionTypeModel;
import kavita.zocial365.app.datas.MySession;
import kavita.zocial365.app.datas.MyURL;
import kavita.zocial365.app.datas.R;
import kavita.zocial365.app.datas.ViewPager.CustomViewPager;
import kavita.zocial365.app.datas.http.GetJson;


/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link kavita.zocial365.app.datas.Detail.AttractionTypesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link kavita.zocial365.app.datas.Detail.AttractionTypesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AttractionTypesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ArrayList<AttractionTypeModel> attractionTypeModels = new ArrayList<AttractionTypeModel>();
    private OnFragmentInteractionListener mListener;
    private CustomViewPager mViewPager;
    DetailActivity.SectionsPagerAdapter mSectionsPagerAdapter;
    private ImageAdapter imageAdapter;
    private ListView listView;
    private DownloadType downloadType;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AttractionTypesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AttractionTypesFragment newInstance(String param1, String param2) {
        AttractionTypesFragment fragment = new AttractionTypesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public AttractionTypesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        mViewPager = ((DetailActivity) getActivity()).mViewPager;
        mSectionsPagerAdapter = ((DetailActivity) getActivity()).mSectionsPagerAdapter;

        downloadType = (DownloadType) new DownloadType().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        if (((DetailActivity) getActivity()).page_name.equalsIgnoreCase(SearchFragment.class.getSimpleName())) {
            AttractionTypeModel attractionTypeModel = new AttractionTypeModel();
            attractionTypeModel.setId("");
            attractionTypeModel.setType_name_th("ทุกประเภท");
            attractionTypeModel.setType_type_nm_t("ALL");

            attractionTypeModels.add(attractionTypeModel);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_attraction_types, container, false);
        listView = (ListView) view.findViewById(R.id.listView_type);
        imageAdapter = new ImageAdapter();
        listView.setAdapter(imageAdapter);
        //mSectionsPagerAdapter.notifyDataSetChanged();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ((DetailActivity) getActivity()).type = attractionTypeModels.get(position).getId();
                ((DetailActivity) getActivity()).type_name = attractionTypeModels.get(position).getType_name_th();
                ((DetailActivity) getActivity()).mViewPager.setCurrentItem(4);


            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
//
//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    class ImageAdapter extends BaseAdapter {
        ImageAdapter() {

        }

        @Override
        public int getCount() {
            return attractionTypeModels.size();
        }

        @Override
        public Object getItem(int position) {
            return attractionTypeModels.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getActivity().getLayoutInflater().inflate(R.layout.layout_attraction_type, null, false);
            TextView textView = (TextView) view.findViewById(R.id.textViewTitle);
            textView.setText(attractionTypeModels.get(position).getType_name_th());
            TextView textView7 = (TextView) view.findViewById(R.id.textView7);
            textView7.setText(attractionTypeModels.get(position).getType_type_nm_t());
            return view;
        }
    }

    class DownloadType extends AsyncTask<Void, Void, Boolean> {

        public static final String TYPE_TYPE_NM_T = "type_type_nm_t";
        public static final String TYPE_NAME_TH = "type_name_th";
        public static final String ID = "id";
        public static final String OBJECTS = "objects";
        public static final String S9200 = "9200";
        public static final String STATUS = "status";

        @Override
        protected Boolean doInBackground(Void... params) {
            GetJson getJson = new GetJson();
            String url = MyURL.HOST_NAME+"attraction_types.json?auth_token="
                    + MySession.getInstance().getUserModel().getAuth_token();
            String result = getJson.getJSON(url);
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(result);
                if (jsonObject.optString(STATUS).equalsIgnoreCase(S9200)) {
                    JSONArray jsonArray = jsonObject.optJSONArray(OBJECTS);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                        AttractionTypeModel attractionTypeModel = new AttractionTypeModel();
                        attractionTypeModel.setId(jsonObject1.optString(ID));
                        attractionTypeModel.setType_name_th(jsonObject1.optString(TYPE_NAME_TH));
                        attractionTypeModel.setType_type_nm_t(jsonObject1.optString(TYPE_TYPE_NM_T));
                        attractionTypeModels.add(attractionTypeModel);
                    }

                    return true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            imageAdapter.notifyDataSetChanged();
        }
    }
}
