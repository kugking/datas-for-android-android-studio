package kavita.zocial365.app.datas.http;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import kavita.zocial365.app.datas.dao.BaseDao;
import kavita.zocial365.app.datas.dao.EvaluationSummaryCollectionsDAO;
import kavita.zocial365.app.datas.utils.Contextor;
import kavita.zocial365.app.datas.utils.Utils;


/**
 * Created by nuuneoi on 11/16/2014.
 */
public class HTTPEngine {

    private static HTTPEngine instance;

    public static HTTPEngine getInstance() {
        if (instance == null)
            instance = new HTTPEngine();
        return instance;
    }

    private Context mContext;
    private OkHttpClient client = new OkHttpClient();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();

    private HTTPEngine() {
        mContext = Contextor.getInstance().getContext();
    }


    private Map<String, String> getBaseData() {
        Map<String, String> postParams = new HashMap<String, String>();
        postParams.put("device_id", Utils.getInstance().getDeviceId());
        postParams.put("version", Utils.getInstance().getVersionName());
        return postParams;
    }

    public <T extends BaseDao> Call loadPostUrl(String url, Map<String, String> postData, final HTTPEngineListener<T> listener, final Class<T> tClass) {
        Log.i("URL", url);
        Map<String, String> postParams = getBaseData();
        if (postData != null) {
            for (Map.Entry<String, String> entry : postData.entrySet()) {
                postParams.put(entry.getKey(), entry.getValue());
            }
        }
        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded; charset=utf-8");
        RequestBody body = RequestBody.create(mediaType, mapToPostString(postParams));

        Request request = new Request.Builder()
                .url(url)
                        // .post(body)
                .build();

        client.setConnectTimeout(30, TimeUnit.SECONDS);
        client.setReadTimeout(30, TimeUnit.SECONDS);
        Call call = client.newCall(request);

        HTTPRequestData data = new HTTPRequestData();
        data.url = url;
        data.postData = postData;
        data.call = call;

        new HTTPRequestTask(new HTTPRequestListener() {
            @Override
            public void onMessageReceived(String message) {
                Log.i("Message :", message);
                if (listener != null) {
                    String resp = message;
                    try {
                        T data = gson.fromJson(resp, tClass);
                        listener.onResponse(data, resp);
                    } catch (Exception e) {
                        e.printStackTrace();
                        T data = null;
                        try {
                            data = tClass.newInstance();
//                            data.setSuccess(false);
//                            data.setReason("Cannot parse JSON");
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                        try {
                            listener.onFailure(data, resp);
                        } catch (Exception e2) {
                            listener.onFailure(data, "");
                            e2.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onMessageError(int statusCode, String message) {
                Log.i("Message Error:" + statusCode, message);
                if (listener != null) {
                    T data = null;
                    try {
                        data = tClass.newInstance();
//                        data.setSuccess(false);
//                        data.setReason(statusCode + "");
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    listener.onResponse(data, message);
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, data);

        return call;
    }

    public <T extends BaseDao> Call loadDeleteUrl(String url, Map<String, String> postData, final HTTPEngineListener<T> listener, final Class<T> tClass, Activity activity) {
        Log.i("URL", url);
        Map<String, String> postParams = getBaseData();
        if (postData != null) {
            for (Map.Entry<String, String> entry : postData.entrySet()) {
                postParams.put(entry.getKey(), entry.getValue());
            }
        }
        MediaType mediaType = MediaType.parse("charset=utf-8");
        RequestBody body = RequestBody.create(mediaType, mapToPostString(postParams));

        Request request = new Request.Builder()
                .url(url)
                .delete()
                        // .post(body)

                .build();

        client.setConnectTimeout(30, TimeUnit.SECONDS);
        client.setReadTimeout(30, TimeUnit.SECONDS);
        Call call = client.newCall(request);

        HTTPRequestData data = new HTTPRequestData();
        data.url = url;
        data.postData = postData;
        data.call = call;

        new HTTPRequestTask(new HTTPRequestListener() {
            @Override
            public void onMessageReceived(String message) {
                Log.i("Message :", message);
                if (listener != null) {
                    String resp = message;
                    try {
                        T data = gson.fromJson(resp, tClass);
                        listener.onResponse(data, resp);
                    } catch (Exception e) {
                        e.printStackTrace();
                        T data = null;
                        try {
                            data = tClass.newInstance();
//                            data.setSuccess(false);
//                            data.setReason("Cannot parse JSON");
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                        try {
                            listener.onFailure(data, resp);
                        } catch (Exception e2) {
                            listener.onFailure(data, "");
                            e2.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onMessageError(int statusCode, String message) {
//                Log.i("Message Error:" + statusCode, message);
                if (listener != null) {
                    T data = null;
                    try {
                        data = tClass.newInstance();
//                        data.setSuccess(false);
//                        data.setReason(statusCode + "");
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    listener.onResponse(data, message);
                }
            }
        }, activity).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, data);

        return call;
    }

    private String mapToPostString(Map<String, String> data) {
        StringBuilder content = new StringBuilder();
        for (Map.Entry<String, String> entry : data.entrySet()) {
            if (content.length() > 0) {
                content.append('&');
            }
            try {
                content.append(URLEncoder.encode(entry.getKey(), "UTF-8"))
                        .append('=')
                        .append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                throw new AssertionError(e);
            }
        }
        return content.toString();
    }

    public Call loadEvaluation(final HTTPEngineListener<EvaluationSummaryCollectionsDAO> listener, String url) {
        Map<String, String> postData = new HashMap<String, String>();
        Log.i("loadEvaluation", "" + url);
        return loadPostUrl(url + "", postData, listener, EvaluationSummaryCollectionsDAO.class);
    }

    public Call deleteImage(final HTTPEngineListener<BaseDao> listener, String url, Activity activity) {
        Map<String, String> postData = new HashMap<String, String>();
        Log.i("loadEvaluation", "" + url);
        return loadDeleteUrl(url + "", postData, listener, BaseDao.class, activity);
    }
}